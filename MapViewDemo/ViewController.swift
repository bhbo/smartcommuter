//
//  ViewController.swift
//  MapViewDemo
//
//  Created by Ravi Shankar on 06/07/15.
//  Copyright (c) 2015 Ravi Shankar. All rights reserved.
//


//eieieiei33
import UIKit
import MapKit
import CoreLocation
var path:[[(line:Int?,node:Int,time:Double)]]?
var selectedPath:[(line:Int?,node:Int,time:Double)]?
var noOfCal:Int?
var test=0
protocol HandleMapSearch {
    func dropPinZoomIn(sta:Place)
}


var counter=0 //couting no. of lines for overlaying tube line on the map
var from_stationReceived = 0
var to_stationReceived = 0



class ViewController: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate,HandleSelectedPath{
    
    var locationManager: CLLocationManager = CLLocationManager()
    var startLocation: CLLocation!
    
    var selectedPath:[(line:Int?,node:Int,time:Double)]? = nil
    
    var selectedPin:Place? = nil
    
    var routePreviewPage: RoutePreviewController!
    var trackingPage : TrackingController!
    
    @IBOutlet weak var from_station: UITextField!
    
    @IBOutlet weak var mapView: MKMapView!
    
    @IBOutlet weak var to_station: UITextField!

    @IBOutlet weak var searchButt: UIButton!
    
    var resultSearchController:UISearchController? = nil
    
    
    //- add tableview of routeplan
    //-@IBOutlet weak var tableView: UITableView!
    //- add
    //-var object: NSMutableArray! = NSMutableArray()
    
    func selectedPathDraw(selectedRoute:[(line:Int?,node:Int,time:Double)]){
        print("7687980")
        selectedPath = selectedRoute
        routeDraw()
    }
    
    @IBAction func clearButtonAction(sender: AnyObject) {
        mapView.removeAnnotations(mapView.annotations)
        mapView.removeOverlays(mapView.overlays)
        counter=0
        from_stationReceived=0
        to_stationReceived=0
        from_station.text=""
        to_station.text=""
        viewDidLoad()
    }
    
    
    @IBAction func currentLocationButt(sender: AnyObject) {
        //to be done...
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()

    }
    
    func calDistanceBetweenTwoPoints(x1:Double,y1:Double,x2:Double,y2:Double)->Double{
        //return sqrt(((x2 - x1)*(x2 - x1)) + ((y2 - y1)*(y2 - y1)))
        let lonA = Double(x1)*(M_PI/180)
        let lonB = Double(x2)*(M_PI/180)
        let latA = Double(y1)*(M_PI/180)
        let latB = Double(y2)*(M_PI/180)
        
        
        // haversine formula
        
        let dlon = lonB - lonA
        let dlat = latB - latA
        let a = pow(sin(dlat/2),2.0) + cos(latA) * cos(latB) * pow(sin(dlon/2),2)
        let c = 2 * asin(sqrt(a))
        let r = 6371.0
        return c*r

    }
   
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        CLGeocoder().reverseGeocodeLocation(manager.location!, completionHandler: {(placemarks, error)->Void in
            var latestLocation: AnyObject = locations[locations.count - 1]
            if (error != nil) {
                print("Reverse geocoder failed with error" + error!.localizedDescription)
                return
            }
            
            if locations.count == 1 {
                
                let sta_location = CLLocationCoordinate2D(latitude: latestLocation.coordinate.latitude, longitude: latestLocation.coordinate.longitude)
                
                self.nearestStation(sta_location)
                self.locationManager.stopUpdatingLocation()

            } else {
                print("Problem with the data received from geocoder")
            }
        })
    }
    
    func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
        print("Error while updating location " + error.localizedDescription)
    }
    
    func nearestStation(sta_location:CLLocationCoordinate2D) ->Station{
        
        
        //find nearest station
        var smallest = 10000000000.0
        var nearestStation:Station? = nil
        for (staKey,staValue) in stations{
            let distance = self.calDistanceBetweenTwoPoints(sta_location.latitude, y1: sta_location.longitude, x2: staValue.latitude, y2:staValue.longitude)
            
            if distance < smallest{
                smallest = distance
                nearestStation = staValue
            }
        }
        
        var annotationView:MKPinAnnotationView!
        let pointAnnotation = CustomPointAnnotation()
        pointAnnotation.pinCustomImageName = "current-pin-1"
        pointAnnotation.coordinate = sta_location
        
        pointAnnotation.title = "Nearest station : \(nearestStation!.title)"
        let x = smallest*1000
        let y = Double(round(1000*x)/1000)
        
        pointAnnotation.subtitle = "\(String(y))m."
        
        
        annotationView = MKPinAnnotationView(annotation: pointAnnotation, reuseIdentifier: "pin")
        self.mapView.addAnnotation(annotationView.annotation!)
        let span = MKCoordinateSpanMake(0.05, 0.05)
        let region = MKCoordinateRegionMake(sta_location, span)
        self.mapView.setRegion(region, animated: true)
        
        self.mapView.selectAnnotation(annotationView.annotation!, animated: true)
        
        return nearestStation!
        
    }
    
    
    
    @IBAction func showRoutePreview(sender: AnyObject) {
       
        self.performSegueWithIdentifier("next", sender: sender)
    }
    
    func routeDraw(){
        counter = (-1)
        
        
        var routeStations : [Station2] = []
        
        //-----------------------------------------RouteDRAWNING-------------------------------------------------//
        
        for (var i = 0;i < selectedPath!.count;i++){
            var value = stations[selectedPath![i].node]
            let lat = value?.latitude
            let long = value?.longitude
            let annotation = Station2(latitude: lat!, longitude: long!)
            annotation.title = value!.title as? String
            routeStations.append(annotation)
        }
        
        
        var pointAnnotation:CustomPointAnnotation!
        
        var annotationView:MKPinAnnotationView!
        
        // Add mappoints to Map
        
        for j in routeStations{
            pointAnnotation = CustomPointAnnotation()
            pointAnnotation.pinCustomImageName = "tube"
            pointAnnotation.coordinate = j.coordinate
            pointAnnotation.title = j.title! as String
            print(j.title)
            annotationView = MKPinAnnotationView(annotation: pointAnnotation, reuseIdentifier: "pin")
            mapView.addAnnotation(annotationView.annotation!)
            
        }
        
        //mapView.addAnnotations(ano)
        
        mapView.delegate = self
        
        // Connect all the mappoints using Poly line.
        
        
        
        var points: [CLLocationCoordinate2D] = [CLLocationCoordinate2D]()
        
        for station in routeStations {
            points.append(station.coordinate)
        }
        
        let polyline = MKPolyline(coordinates: &points, count: points.count)
        
        mapView.addOverlay(polyline)
    }
    
   
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        
        
        //routePreviewPage = storyboard!.instantiateViewControllerWithIdentifier("RoutePreviewController") as! RoutePreviewController
        //routePreviewPage.delegate = self
        
        trackingPage = storyboard!.instantiateViewControllerWithIdentifier("TrackingController") as! TrackingController
        trackingPage.delegate = self
        
        let searchTable = storyboard!.instantiateViewControllerWithIdentifier("SearchTable") as! SearchTable
        resultSearchController = UISearchController(searchResultsController: searchTable)
        resultSearchController?.searchResultsUpdater = searchTable
        
        let searchBar = resultSearchController!.searchBar
        
        searchBar.sizeToFit()
        
 
        
        
        searchBar.placeholder = "Search for start place"
        navigationItem.titleView = resultSearchController?.searchBar
        resultSearchController?.hidesNavigationBarDuringPresentation = false
        resultSearchController?.dimsBackgroundDuringPresentation = true
        definesPresentationContext = true
        searchTable.mapView = mapView
        resultSearchController!.searchBar.scopeButtonTitles = ["All", "Station", "Place"]
        //searchTable.tableView.tableHeaderView = resultSearchController!.searchBar
        
        let values = Array(stations.values)
        
        
        searchTable.handleMapSearchDelegate = self
        searchTable.allTableData = values
        
        zoomToRegion()
        
        let annotations = getMapAnnotations()
        
        var pointAnnotation:CustomPointAnnotation!
        
        var annotationView:MKPinAnnotationView!
        
        // Add mappoints to Map
        for ano in annotations{
            
            for j in ano{
                pointAnnotation = CustomPointAnnotation()
                pointAnnotation.pinCustomImageName = "circle2-2"
                pointAnnotation.coordinate = CLLocationCoordinate2D(latitude: j.latitude, longitude: j.longitude)
                pointAnnotation.title = j.title as String
                annotationView = MKPinAnnotationView(annotation: pointAnnotation, reuseIdentifier: "pin")
                mapView.addAnnotation(annotationView.annotation!)
                
            }

            
            //mapView.addAnnotations(ano)
        }
        mapView.delegate = self
        
        // Connect all the mappoints using Poly line.
        
        for i in annotations{
            
            var points: [CLLocationCoordinate2D] = [CLLocationCoordinate2D]()
            
            for annotation in i {
                points.append(annotation.coordinate)
            }
            
            let polyline = MKPolyline(coordinates: &points, count: points.count)
            
            mapView.addOverlay(polyline)
            
        }
        
        
        
        //self.object.addObject("Plan 1")
        //self.object.addObject("Plan 2")
        //self.object.addObject("Plan 3")
        
        //self.tableView.reloadData()
        
        
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    //Calling Astar Algorithm
    func journey(from_station:String, to_station:String) ->[[(line:Int?,node:Int,time:Double)]]{
        
        var startNode=0, goalNode=0
        noOfCal=1
        for (node,sta) in stations{
            if sta.title == from_station{
                startNode = node
            }
            
            if sta.title == to_station{
                goalNode = node
            }
        }
        print("start: \(startNode) goal: \(goalNode)");
        var path1 = [[(line:Int?,node:Int,time:Double)]]()
        var shorttest_path = MinDistanceSearch(node_table: stations,line_table: lines)
        for(var i=0;i<3;i++){
            path1.append(shorttest_path.shortestPathSearch(startNode,goal_id:goalNode))
            noOfCal = noOfCal!+1
        }
        
    //iiQQQ
        var totalTime: Double = 0;
        for var j in (0..<3){
            print("Path no.\(j)")
        for (var i = 0;i < path1[j].count;i++){
            totalTime+=path1[j][i].time;
            if i>=1{
                
                print("Using \(lines[path1[j][i].line!]!.name) line to \(stations[path1[j][i].node]!.title)")}
        }}
        
        return path1
        
    }
    
    //MARK:- Zoom to region
    func zoomToRegion() {
        
        let location = CLLocationCoordinate2D(latitude: 51.51022, longitude: -0.13392)
        
        let region = MKCoordinateRegionMakeWithDistance(location, 5000.0, 7000.0)
        
        mapView.setRegion(region, animated: true)
    }
    
    //MARK:- Annotations
    
    func getMapAnnotations() -> [[Station]] {
        
        var annotations:Array = [[Station]]()
        
        for (var i=0;i<lineDict.count;i++){
            var an:Array = [Station]()
            //print(sta)
            for (var j=0;j<lineDict[i].count;j += 1){
                var value = stations[lineDict[i][j]]
                an.append(value!)
            }
            annotations.append(an)
        }
        return annotations
    }
    
    
    //func showAnnotations(_ annotations: [AnyObject]!,animated animated: Bool)
    
    
    
    //MARK:- MapViewDelegate methods
    
    func mapView(mapView: MKMapView,viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView?{
        
        let reuseIdentifier = "pin"
        
        var v = mapView.dequeueReusableAnnotationViewWithIdentifier(reuseIdentifier)
        if v == nil {
            v = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseIdentifier)
            v!.canShowCallout = true
        }
        else {
            v!.annotation = annotation
        }
        
        let customPointAnnotation = annotation as! CustomPointAnnotation
        v!.image = UIImage(named:customPointAnnotation.pinCustomImageName)
        
        return v
    }
    
    func mapView(mapView: MKMapView, rendererForOverlay overlay: MKOverlay) -> MKOverlayRenderer {
        
        let polylineRenderer = MKPolylineRenderer(overlay: overlay)
        
        if overlay is MKPolyline {
            if counter == 1{
                //Central
                polylineRenderer.strokeColor = UIColor(hue: 0, saturation: 0.8, brightness: 0.92, alpha: 0.7)
                polylineRenderer.lineWidth = 2
                
            }
            else if counter==4{
                //Jubilee
                polylineRenderer.strokeColor = UIColor(hue: 0.575, saturation: 0.06, brightness: 0.63, alpha: 0.7)
                polylineRenderer.lineWidth = 2
                
            }
            else if counter==6{
                //Nothern
                polylineRenderer.strokeColor = UIColor(hue: 0.9611, saturation: 0.11, brightness: 0.13, alpha: 0.7)
                polylineRenderer.lineWidth = 2
            }
                
            else if counter==8{
                //Piccadilly
                polylineRenderer.strokeColor = UIColor(hue: 0.6139, saturation: 0.78, brightness: 0.57, alpha: 0.7)
                polylineRenderer.lineWidth = 2
            }
                
            else if counter==7{
                //Victoria
                polylineRenderer.strokeColor = UIColor(hue: 0.55, saturation: 0.88, brightness: 0.85, alpha: 0.7)
                polylineRenderer.lineWidth = 2
            }
                
            else if counter==3{
                //District
                polylineRenderer.strokeColor = UIColor(hue: 0.4028, saturation: 0.87, brightness: 0.51, alpha: 0.7)
                polylineRenderer.lineWidth = 2
            }
                
            else if counter==0{
                //Bakerloo
                polylineRenderer.strokeColor = UIColor(hue: 0.075, saturation: 0.82, brightness: 0.69, alpha: 0.7)
                polylineRenderer.lineWidth = 2
            }
            else if counter==2{
                //Circle
                polylineRenderer.strokeColor = UIColor(hue: 0.1278, saturation: 0.81, brightness: 1, alpha: 0.7)
                polylineRenderer.lineWidth = 2
            }
                
            else if counter==5{
                //Metro
                polylineRenderer.strokeColor = UIColor(hue: 0.8194, saturation: 0.8, brightness: 0.45, alpha: 0.7)
                polylineRenderer.lineWidth = 2
            }
                
                
            else if counter == (-1){
                //Route
                polylineRenderer.strokeColor = UIColor(hue: 0.0778, saturation: 0.81, brightness: 0.95, alpha: 1.0)
                polylineRenderer.lineDashPattern = [1,8]
                polylineRenderer.lineWidth = 5
            }
            
            else if counter == 9 {
                polylineRenderer.strokeColor = UIColor(hue: 0.8944, saturation: 0.35, brightness: 0.88, alpha: 1.0)
                polylineRenderer.lineWidth = 2
            }
            
            
            
            
            
        

        }
        
        counter+=1
        return polylineRenderer
    }
}


extension ViewController: HandleMapSearch {
    func dropPinZoomIn(sta:Place){
        // cache the pin
        
        selectedPin = sta
     
        let sta_location = CLLocationCoordinate2D(latitude: selectedPin!.latitude, longitude: selectedPin!.longitude)
        var annotationView:MKPinAnnotationView!
        
        let pointAnnotation = CustomPointAnnotation()
        pointAnnotation.pinCustomImageName = "pin-5"
        pointAnnotation.coordinate = sta_location
        pointAnnotation.title = selectedPin?.title
        
        annotationView = MKPinAnnotationView(annotation: pointAnnotation, reuseIdentifier: "pin")
        mapView.addAnnotation(annotationView.annotation!)
        let span = MKCoordinateSpanMake(0.05, 0.05)
        let region = MKCoordinateRegionMake(sta_location, span)
        mapView.setRegion(region, animated: true)

        if selectedPin?.type=="Place"{
            let searchBar = resultSearchController!.searchBar
            if from_stationReceived==0{
                var nearestStation = self.nearestStation(sta_location)
                
                from_station.text=nearestStation.title
                from_stationReceived=1
            }
            else{
                var nearestStation = self.nearestStation(sta_location)
                to_station.text = nearestStation.title
                to_stationReceived=1
            }
            searchBar.placeholder = "Search for destination place"
            
            if from_stationReceived==1 && to_stationReceived==1{
                path = journey(from_station.text!,to_station: to_station.text!)
            }

            
        }
        // clear existing pins
        //mapView.removeAnnotations(mapView.annotations)
        else{
        let searchBar = resultSearchController!.searchBar
        if from_stationReceived==0{
            from_station.text=selectedPin?.title
            from_stationReceived=1
        }
        else{
            to_station.text=selectedPin?.title
            to_stationReceived=1
        }
        searchBar.placeholder = "Search for destination place"
        
        if from_stationReceived==1 && to_stationReceived==1{
            path = journey(from_station.text!,to_station: to_station.text!)
            
        }
        }
        
    }
    
}



var lineDict = [[48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,24,40,13,21,10,9,64,65,66],//bakerloo
    [67,68,69,70,71,72,73,75,74,75,76,77,78,79,80,1,17,16,15,14,13,12,44,45,35,33,30,81,82,83,84,85,86,87,88,98,97,96,95,94,93,92,91,90,89,85,86,87,88,99,100,101,102,103], //Central
    [23,61,22,1,2,3,4,5,6,7,8,9,39,38,37,34,32,31,36,30,29,28,27,47,26,25,24,23,61,191,190,108,107,106,105,104], //circle
    [74,109,110,111,112,113,114,113,112,111,115,116,117,104,118,119,120,121,120,122,123,124,125,126,127,128,129,128,127,126,125,124,123,122,120,2,1,22,61,23,61,22,1,2,120,3,4,5,6,9,34,32,31,130,131,132,133,134,135,136,137,138,139,140,141,142,143,144,145,146,147],//district
    [148,149,150,151,152,153,154,155,156,157,158,159,160,14,20,8,64,161,162,163,164,165,166,167,136,83], //Jubilee
    [168,169,170,171,172,175,173,174,173,175,176,177,178,179,180,188,187,186,185,184,183,182,181,182,183,184,185,186,187,188,189,152,158,24,25,26,47,27,28,29,30,36], //metro
    [243,242,241,240,238,239,238,237,236,235,234,233,231,230,229,228,227,226,225,224,223,222,223,224,225,226,227,228,229,230,231,232,266,47,244,245,29,33,32,162,246,66,247,66,246,162,32,33,29,245,244,47,266,41,42,12,11,10,9,64,247,248,249,250,251,252,253,254,255,256,257,258], //northern
    [265,264,263,262,213,261,47,41,13,20,6,266,259,249,268], // Victoria
    [181,182,183,184,185,186,192,193,194,195,196,197,198,109,110,199,200,201,202,203,204,205,206,207,208,206,205,204,203,202,201,200,199,110,115,104,118,120,3,4,18,19,20,21,11,46,44,43,47,210,211,212,213,214,215,216,217,218,219,220,221],//picca,
    [104,105,106,107,108,190,191,61,23,24,25,26,47,27,28,29,30,130,131,132,133,134,135,136,137,267,138,139]//hammersmith
    ]


var stationDict = [1 : ["Notting Hill Gate", 51.509028, -0.1962847],
                   2 : ["High Street Kensington", 51.5003459144, -0.1923516640],
                   3 : ["Gloucester Road", 51.49408266, -0.17295341],
                   4 : ["South Kensington", 51.494066, -0.172791],
                   5 : ["Sloane Square", 51.49258474, -0.156090904],
                   6 : ["Victoria", 51.49662869, -0.14400853],
                   7 : ["St.James's Park", 51.49971, -0.13394],
                   8 : ["Westminster", 51.50121, -0.12489],
                   9 : ["Embankment", 51.50717, -0.12195],
                   10 : ["Charing Cross", 51.507108, -0.122963],
                   11 : ["Leicester Square", 51.51148, -0.12849],
                   12 : ["Tottenham Court Road", 51.51640, -0.13027],
                   13 : ["Oxford Circus", 51.51517, -0.14119],
                   14 : ["Bond Street", 51.51461, -0.14897],
                   15 : ["Marble Arch", 51.5135970, -0.15869195],
                   16 : ["Lancaster Gate", 51.512083, -0.175067],
                   17 : ["Queensway", 51.510484, -0.187050],
                   
                   18 : ["Knightsbridge", 51.50169, -0.16030],
                   19 : ["Hyde Park Corner", 51.50313, -0.15278],
                   20 : ["Green Park", 51.50674, -0.14276],
                   21 : ["Piccadilly Circus", 51.51022, -0.13392],
                   
                   22 : ["Bayswater", 51.51224, -0.187569],
                   23 : ["Edgware Road", 51.520195, -0.166986],
                   24 : ["Baker Street", 51.52265, -0.15704],
                   25 : ["Great Portland Street", 51.52391,-0.14397],
                   26 : ["Euston Square", 51.52584, -0.13570],
                   27 : ["Farringdon", 51.520362948, -0.105126482],
                   28 : ["Barbican", 51.520865, -0.097758],
                   29 : ["Moorgate", 51.51817, -0.08859],
                   30 : ["Liverpool Street", 51.517675, -0.0824580],
                   31 : ["Tower Hill", 51.509910, -0.076813],
                   32 : ["Monument", 51.510165, -0.085991],
                   33 : ["Bank", 51.5134047, -0.08905843],
                   34 : ["Cannon Street", 51.51141,-0.09047],
                   35 : ["St.Paul's", 51.515285, -0.097598],
                   36 : ["Aldgate", 51.51394, -0.07537],
                   37 : ["Mansion House", 51.51256, -0.09397],
                   38 : ["Blackfriars", 51.5114403, -0.10419050],
                   39 : ["Temple", 51.51114, -0.11341],
                   
                   40 : ["Regent's Park", 51.52344, -0.14713],
                   41 : ["Warran Street", 51.52450, -0.13810],
                   42 : ["Goodge Street", 51.52060, -0.13441],
                   43 : ["Russell Sqaure", 51.523243, -0.124336],
                   44 : ["Holborn", 51.51711, -0.12055],
                   45 : ["Chancery Lane", 51.51836, -0.11115],
                   46 : ["Covent Garden", 51.51308, -0.12427],
                   47 : ["King's Cross St.Pancras", 51.53057, -0.12399],
                   
]


var lines = [1 : Line(name: "Bakerloo"),
             2 : Line(name: "Central"),
             3 : Line(name: "Circle"),
             4 : Line(name: "District"),
             5 : Line(name: "Hammersmith & City"),
             6 : Line(name: "Jubilee"),
             7 : Line(name: "Metropolitan"),
             8 : Line(name: "Northern"),
             9 : Line(name: "Piccadilly"),
             10 : Line(name: "Victoria")
]



//var nottingHill = Station(title: "Notting Hill Gate", latitude: 51.509028, longitude: -0.1962847, edg: [(line:2, node:17), (line:3, node:2)])




var stations : Dictionary<Int,Station> = [1 : Station(title: "Notting Hill Gate", latitude: 51.509028, longitude: -0.1962847, edg: [(line:2, node:17, time:1.5), (line:3, node:2, time:2), (line: 4,node:2, time:2), (line:3,node:22, time:1), (line: 4,node:22, time:1), (line:2, node:80,time:1)], zone:1),
                                          2 : Station(title: "High Street Kensington", latitude: 51.5003459144, longitude: -0.1923516640, edg: [(line:3, node:1, time:2), (line: 4,node:1, time:2), (line:3, node:3, time:2.5), (line:4 ,node:120, time:1)], zone:1),
                                          3 : Station(title: "Gloucester Road", latitude: 51.49408266, longitude: -0.17295341, edg: [(line:3, node:4, time:2),(line:3, node:2, time:2.5),(line:9, node:4,time: 2), (line:4 ,node:4, time:1), (line:4,node:120,time:1),(line:9,node:120,time:1)], zone:1),
                                          4 : Station(title: "South Kensington", latitude: 51.494066, longitude: -0.172791, edg: [(line:3, node:3, time:2), (line:3, node:5, time:2.5),(line:9, node:3, time:2), (line:9, node:18, time:2.5), (line: 4,node:3, time:2), (line:4 ,node:5, time:2.5)], zone:1),
                                          5 : Station(title: "Sloane Square", latitude: 51.49258474, longitude: -0.156090904, edg: [(line:3, node:4, time:2.5), (line:3, node:6,time:2.5), (line:4 ,node:4, time:2.5), (line: 4,node:6, time:2.5)], zone:1),
                                          6 : Station(title: "Victoria", latitude: 51.49662869, longitude: -0.14400853, edg: [(line:3, node:5,time:2.5), (line:3, node:7, time:2), (line:4 ,node:5, time:2.5), (line:4 ,node:8, time:1), (line:10, node:20, time:2), (line: 10,node:260, time:1)], zone:1),
                                          7 : Station(title: "St.James's Park", latitude: 51.49971, longitude: -0.13394, edg: [(line:3, node:6, time:2), (line:3, node:8, time:1.5)], zone:1),
                                          8 : Station(title: "Westminster", latitude: 51.50121, longitude: -0.12489, edg: [(line:3, node:7, time:2), (line:3, node:9, time:1.5), (line:6, node:20, time:2),(line:6,node:64,time:1)], zone:1),
                                          9 : Station(title: "Embankment",latitude: 51.50717, longitude: -0.12195, edg: [(line:3, node:8, time:1.5), (line:8, node:10, time:1),(line:1,node:64,time:1), (line:8 ,node:64, time:1), (line:3, node:39, time:2), (line: 4,node:8, time:1.5), (line:4 ,node:34, time:1)], zone:1),
                                          10 : Station(title: "Charing Cross", latitude: 51.507108, longitude: -0.122963, edg: [(line:8, node:9, time:1), (line:8, node:11, time:1.5), (line:1, node:21, time:2), (line:1, node:9, time:2)], zone:1),
                                          11 : Station(title: "Leicester Square", latitude: 51.51148, longitude: -0.12849, edg: [(line:8, node:10, time:1.5), (line:8, node:12, time:1), (line:9, node:21, time:1.5), (line: 9,node:46, time:1)], zone:1),
                                          12 : Station(title: "Tottenham Court Road", latitude: 51.51640, longitude: -0.13027, edg: [(line:8, node:11, time:1), (line:2, node:13, time:1.25), (line:2 ,node:44, time:1), (line: 8,node:42, time:1)], zone:1),
                                          13 : Station(title: "Oxford Circus", latitude: 51.51517, longitude: -0.14119, edg: [(line:2, node:12, time:1.25), (line:1 ,node:40, time:1), (line: 10,node:41, time:1), (line:2, node:14, time: 1.5), (line:1, node:21, time:2), (line:10, node:20,time:2)], zone:1),
                                          14 : Station(title: "Bond Street", latitude: 51.51461, longitude: -0.14897, edg: [(line:2, node:13, time:1.5), (line:2, node:15, time:1.5), (line:6, node:20, time:2), (line:6,node:24,time:1)], zone:1),
                                          15 : Station(title: "Marble Arch", latitude: 51.5135970, longitude: -0.15869195, edg: [(line:2, node:14, time:1.5), (line:2, node:16, time:1.75)], zone:1),
                                          16 : Station(title: "Lancaster Gate", latitude: 51.512083, longitude: -0.175067, edg: [(line:2, node:15, time:1.75), (line:2, node:17,time:2)], zone:1),
                                          17 : Station(title: "Queensway", latitude: 51.510484, longitude: -0.187050, edg: [(line:2, node:16,time:2), (line:2, node:1, time:1.5)], zone:1),
                                          18 : Station(title: "Knightsbridge", latitude: 51.50169, longitude: -0.16030, edg: [(line:9, node:4, time:2.5), (line:9, node:19, time:1.5)], zone:1),
                                          19 : Station(title: "Hyde Park Corner", latitude: 51.50313, longitude: -0.15278, edg: [(line:9, node:18, time:1.5), (line:9, node:20, time:2.5)], zone:1),
                                          20 : Station(title: "Green Park", latitude: 51.50674, longitude: -0.14276, edg: [(line:9, node:19, time:2.5), (line:10, node:6, time:2), (line:6, node:8, time:2), (line:9, node:21, time:1.5), (line:10, node:13, time:2), (line:6, node:14, time:2)], zone:1),
                                          21 : Station(title: "Piccadilly Circus", latitude: 51.51022, longitude: -0.13392, edg: [(line:9, node:20, time:1.5), (line:1, node:10, time:2), (line:9, node:11, time:1.5), (line:1, node:13, time:2)], zone:1),
                                            
                                            
                                            22 : Station(title: "Bayswater", latitude: 51.51224, longitude: -0.187569, edg: [(line:3, node:23,time:5), (line:4 ,node:23, time:5), (line: 4,node:1, time:2), (line:3, node:1, time: 2)], zone:1),
                                            23 : Station(title: "Edgware Road", latitude: 51.520195, longitude: -0.166986, edg: [(line:3, node:22, time:5), (line: 4,node:22, time:5), (line:3, node:24, time:2)], zone:1),
                                            24 : Station(title: "Baker Street", latitude: 51.52265, longitude: -0.15704, edg: [(line:3,node:23,time:2), (line:1,node:40,time:2), (line:3,node:25,time:2), (line:6, node:14, time:2.25), (line:1,node:63,time:1),(line:7,node:158,time:1), (line:7 ,node:25, time:1), (line: 5,node:3, time:1), (line: 5,node:25, time:1), (line: 6,node:14, time:1), (line: 6,node:160, time:1)], zone:1),
                                            25 : Station(title:"Great Portland Street",latitude:51.52391, longitude:-0.14397, edg:[(line:3,node:24,time:2),(line:3,node:26,time:1.5), (line:5,node:24, time:2), (line: 5,node:26, time:1.5), (line: 7,node:24, time:2), (line:7 ,node:26, time:1.5)], zone:1),
                                            26 : Station(title: "Euston Square", latitude:51.52584, longitude:-0.13570, edg:[(line:3,node:25,time:1.5), (line:3,node:47,time:2),(line:5,node:25,time:1.5), (line:5,node:47,time:2),(line:7,node:25,time:1.5), (line:7,node:47,time:2)], zone:1),
                                        27 : Station(title: "Farringdon", latitude:51.520362948, longitude:-0.105126482, edg:[(line:3,node:47,time:3.5), (line:3,node:28,time:1.5),(line:5,node:47,time:3.5), (line:5,node:28,time:1.5),(line:7,node:47,time:3.5), (line:7,node:28,time:1.5)], zone:1),
                                            28 : Station(title: "Barbican", latitude:51.520865, longitude:-0.097758, edg:[(line:3,node:27,time:1.5),(line:3,node:29,time:1.5),(line:5,node:27,time:1.5),(line:5,node:29,time:1.5),(line:7,node:27,time:1.5),(line:7,node:29,time:1.5)], zone:1),
                                            29 : Station(title: "Moorgate", latitude:51.51817, longitude:-0.08859, edg: [(line:3,node:28,time:1.5),(line:3,node:30,time:2),(line:8,node:33,time:2),(line:8,node:32,time:2),(line:8,node:33,time:1),(line:8,node:245,time:1),(line:5,node:28,time:1.5),(line:5,node:30,time:2),(line:7,node:28,time:1.5),(line:7,node:30,time:2)], zone:1),
                                            30 : Station(title: "Liverpool Street", latitude:51.517675, longitude:-0.0824580, edg:[(line:2,node:33,time:2),(line:3,node:36,time:2.5), (line:3, node:29, time:2),(line:5,node:130,time:1), (line:5, node:29, time:2), (line:7, node:29, time:2),(line:7,node:36,time:2.5)], zone:1),
                                            31 : Station(title: "Tower Hill", latitude:51.509910, longitude:-0.076813, edg:[(line:3,node:36,time:2), (line:3,node:32,time:1.5),(line:4,node:130,time:1),(line:4,node:32,time:1.5)], zone:1),
                                            32 : Station(title: "Monument", latitude:51.510165, longitude:-0.085991, edg: [(line:3,node:31,time:1.5),(line:3,node:34,time:1.5),(line:2,node:35,time:1.75), (line:2,node:30,time:2), (line:8,node:29,time:2),(line:4,node:31,time:1.5),(line:4,node:34,time:1.5), (line:8,node:162,time:1)], zone:1),
                                            33 : Station(title: "Bank", latitude:51.5134047, longitude:0.08905843, edg: [(line:3,node:31,time:1.5),(line:3,node:34,time:1.5),(line:2,node:35,time:1.75), (line:2,node:30,time:2), (line:8,node:29,time:2), (line:4,node:31,time:1.5),(line:4,node:34,time:1.5), (line:8,node:162,time:1), (line:11,node:64,time:1)], zone:1),
                                            34 : Station(title: "Cannon Street", latitude:51.51141, longitude:-0.09047, edg: [(line:3,node:32,time:1), (line:3,node:37,time:1.5),(line:4,node:32,time:1),(line:4,node:9,time:1)], zone:1),
                                            35 : Station(title: "St.Paul's", latitude:51.515285, longitude:-0.097598, edg: [(line:2,node:33,time:1.75), (line:2,node:45,time:2)], zone:1),
                                            36 : Station(title: "Aldgate", latitude:51.51394, longitude:-0.07537, edg:[(line:3,node:30,time:2.5),(line:3,node:31,time:2),(line:7,node:30,time:2.5)], zone:1),
                                            37 : Station(title: "Mansion House", latitude:51.51256, longitude:-0.09397, edg:[(line:3,node:34,time:1.5), (line:3,node:38,time:1.5)], zone:1),
                                            38 : Station(title: "Blackfriars", latitude:51.5114403, longitude:-0.10419050, edg:[(line:3,node:37,time:1.5),(line:3,node:39,time:2)], zone:1),
                                            39 : Station(title: "Temple", latitude:51.51114, longitude:-0.11341, edg:[(line:3,node:38,time:2), (line:3,node:9,time:2)], zone:1),
                                            40 : Station(title: "Regent's Park", latitude:51.52344, longitude:-0.14713, edg:[(line:1,node:24,time:2),(line:1,node:13,time:2)], zone:1),
                                            41 : Station(title: "Warran Street", latitude:51.52450, longitude:-0.13810, edg:[(line:8,node:42,time:1.5),(line:10,node:13,time:1.75),(line:8,node:266,time:1),(line:10,node:266,time:1)], zone:1),
                                            42 : Station(title: "Goodge Street", latitude:51.52060, longitude:-0.13441, edg:[(line:8,node:12,time:1.5),(line:8,node:41,time:1.5)], zone:1),
                                            43 : Station(title: "Russell Sqaure", latitude:51.523243, longitude:-0.124336, edg:[(line:9,node:44,time:1.5), (line:9,node:47,time:2)], zone:1),
                                            44 : Station(title: "Holborn", latitude:51.51711, longitude:-0.12055, edg:[(line:2,node:12,time:1.75),(line:2,node:45,time:1.25),(line:9,node:43,time:1.5),(line:9,node:46,time:2)], zone:1),
                                            45 : Station(title: "Chancery Lane", latitude:51.51836, longitude:-0.11115, edg:[(line:2,node:44,time:1.25),(line:2,node:35,time:2)], zone:1),
                                            46 : Station(title: "Covent Garden", latitude:51.51308, longitude:-0.12427, edg:[(line:9,node:44,time:2),(line:9,node:11,time:1)], zone:1),
                                            47 : Station(title: "King's Cross St.Pancras", latitude:51.53057, longitude:-0.12399, edg:[(line:9,node:43,time:2.5),(line:3,node:27,time:3.5),(line:3,node:26,time:2),(line:5,node:27,time:3.5),(line:5,node:26,time:2),(line:7,node:27,time:3.5),(line:7,node:26,time:2),(line:9,node:210,time:1),(line:10,node:261,time:1),(line:10,node:266,time:1),(line:8,node:244,time:1),(line:8,node:266,time:1)], zone:1),

//bakerloo

48 : Station(title: "Harrow & Wealdstone", latitude:51.59205973, longitude: -0.334725352, edg:[(line:1, node:49, time:2.5)], zone:5),
49 : Station(title: "Kenton", latitude: 51.58173809, longitude: -0.316870809, edg:[(line:1, node:48, time:2.5),(line:1, node:50, time:2)], zone:4),
50 : Station(title: "South Kenton", latitude: 51.57044666, longitude: -0.308566354, edg:[(line:1, node:49, time:2), (line:1, node:51, time:2)], zone:4),
51 : Station(title: "North Wembley", latitude: 51.56258091, longitude: -0.304072648, edg:[(line:1, node:50, time:2), (line:1, node:52, time:2)], zone:4),
52 : Station(title: "Wembley Central", latitude: 51.55122817, longitude: -0.29577538, edg:[(line:1, node:51,time:2),(line:1,node:53,time:2.5)], zone:4),
53 : Station(title: "Stonebridge Park", latitude: 51.54402388, longitude: -0.275978856, edg:[(line:1, node:52, time:2.5), (line:1, node:54, time:2)], zone:3),
54 : Station(title: "Harlesden", latitude: 51.53628278, longitude: -0.257622488, edg:[(line:1, node:53, time:2), (line:1, node:55, time:3)], zone:3),
55 : Station(title: "Willesden Junction", latitude: 51.53032031, longitude: -0.229378995, edg:[(line:1 ,node:54, time:3), (line:1, node:56, time:3)], zone:3),
56 : Station(title: "Kensal Green", latitude: 51.53060655, longitude:-0.224253545, edg:[(line:1, node:55,time:3),(line:1, node:57, time:2.5)], zone:2),
57 : Station(title: "Queen's Park", latitude: 51.534179, longitude: -0.205257721, edg:[(line:1, node:56,time:2.5),(line:1, node:58, time:1.5)], zone:2),
58 : Station(title: "Kilburn Park", latitude: 51.53495818, longitude: -0.193963023, edg:[(line:1, node:57, time:1.5),(line:1 , node: 59, time:2)], zone:2),
59 : Station(title: "Maida Vale", latitude: 51.52989409, longitude: -0.185888819, edg:[(line:1, node:58, time:2),(line:1, node:60, time:2)], zone:2),
60 : Station(title: "Warwick Avenue", latitude: 51.52329728, longitude: -0.183777837, edg:[(line:1, node:59, time:2),(line:1, node:61, time:1)], zone:2),

61 : Station(title: "Paddington", latitude: 51.5151846554, longitude: -0.17553880792, edg:[(line:1, node:60, time:2),(line:1, node:62, time:2),(line:3, node:191, time:2),(line:3, node:23, time:3),(line:5, node:191, time:2),(line:5, node:23, time:3),(line:3, node:22, time:2),(line:4, node:23, time:3),(line:4, node:22, time:2)], zone:1),
62 : Station(title: "Edgware Road", latitude: 51.519560, longitude: -0.169068, edg:[(line:1, node:61, time:2), (line:1, node:63, time:1)], zone:1),
63 : Station(title: "Marlybone", latitude : 51.522660, longitude: -0.162996, edg:[(line:1, node:62, time:1), (line:1, node:24, time:1.5)], zone:1),

64 : Station(title: "Waterloo", latitude: 51.50322, longitude: -0.11328, edg:[(line:1, node:9, time:1.5),(line:1, node:65, time:2), (line:8, node:9, time:1.5), (line:6, node:8,time:1.75),(line:6, node:161, time:1.25),(line:8, node:247, time:3.5)], zone:1),
65 : Station(title: "Lambeth North",latitude: 51.49894, longitude: -0.11216, edg:[(line:1,node:64,time:2), (line:1, node:66, time:2)], zone:1),
66 : Station(title: "Elephant & Castle", latitude: 51.49467, longitude: -0.10047, edg:[(line:1,node:65,time:2),(line:8,node:246,time:2),(line:8,node:247,time:2.5)], zone:1),

//Central
67 : Station(title: "West Ruislip", latitude: 51.569324, longitude: -0.436740, edg:[(line:2, node:68, time:2.5)], zone:6),
68 : Station(title: "Ruislip Gardens", latitude: 51.560711, longitude: -0.410587, edg:[(line:2, node:67, time:2.5), (line:2, node:69, time:1.75)], zone:5),
69 : Station(title: "South Ruislip", latitude: 51.556795, longitude: -0.398708, edg:[(line:2, node:68, time:1.75),(line:2, node:70, time:2.5)], zone:5),
70 : Station(title: "Northolt", latitude: 51.548054, longitude: -0.368257,edg:[(line:2, node:69,time:2.5),(line:2, node:71,time:2.25)], zone:5),
71 : Station(title: "Greenford", latitude: 51.542346, longitude: -0.345903,edg:[(line:2, node:70,time:2.25),(line:2, node:72,time:2.25)], zone:4),
72 : Station(title: "Perivale", latitude: 51.536673, longitude: -0.32391, edg:[(line:2, node:71,time:2.25),(line:2, node:73,time:2.5)], zone:4),
73 : Station(title: "Hanger Lane", latitude: 51.5300, longitude: -0.2929,edg:[(line:2, node:72,time:2.5),(line:2, node:76,time:3.25)], zone:3),
74 : Station(title: "Ealing Broadway", latitude: 51.51482, longitude: -0.30118,edg:[(line:2, node:75,time:2.75),(line:4, node:109,time:3)], zone:3),
75 : Station(title: "West Acton",latitude: 51.51818 , longitude: -0.28064, edg:[(line:2, node:74,time:2.75),(line:2, node:76,time:2.75)], zone:3),
76 : Station(title: "North Acton", latitude: 51.52370, longitude: -0.26019, edg:[(line:2, node:73,time:3.25),(line:2, node:75,time:2.75),(line:2, node:77,time:2)], zone:3),
77 : Station(title: "East Acton", latitude: 51.51753, longitude: -0.24827, edg:[(line:2, node:76,time:2),(line:2, node:78,time:2.75)], zone:2),
78 : Station(title: "White City", latitude: 51.512206, longitude: -0.224226, edg:[(line:2, node:77,time:2.75),(line:2, node:79,time:3.25)], zone:2),
79 : Station(title: "Shepherd's Bush", latitude: 51.50474, longitude: -0.21881, edg:[(line:2, node:78,time:3.25),(line:2, node:80,time:1.75)], zone:2),
80 : Station(title: "Holland Park", latitude: 51.50722, longitude: -0.20553, edg:[(line:2, node:79,time:1.75),(line:2, node:1,time:1.5)], zone:2),
//''' in the bottle line'''
81 : Station(title: "Bethnal Green", latitude: 	51.52718 , longitude: -0.05504, edg:[(line:2, node:30,time:3.25),(line:2, node:82,time:2.75)], zone:2),
82 : Station(title: "Mile End", latitude: 51.525306, longitude: -0.033471, edg:[(line:2, node:81,time:2.75),(line:2, node:83,time:3.5),(line:4, node:132,time:2),(line:4, node:134,time:1.5),(line:5, node:132,time:2),(line:5, node:134,time:1.5)], zone:2),
83 : Station(title: "Stratford", latitude: 51.541208, longitude: -0.003776, edg:[(line:2, node:82,time:3.5),(line:2, node:84,time:2.75),(line:6, node:136,time:2.75)], zone:3),
84 : Station(title: "Leyton", latitude: 51.556483, longitude: -0.005669 ,edg:[(line:2, node:83,time:2.75),(line:2, node:85,time:2.25)], zone:3),
85 : Station(title: "Leytonstone", latitude: 51.56828 , longitude: 0.00739, edg:[(line:2, node:84,time:2.25),(line:2, node:86,time:2.75)], zone:3),
86 : Station(title: "Snaresbrook", latitude: 51.58002, longitude: 0.02133,edg:[(line:2, node:85,time:2.75),(line:2, node:87,time:2)], zone:4),
87 : Station(title: "South Woodford", latitude: 51.59088, longitude: 0.02726, edg:[(line:2, node:86,time:2),(line:2, node:88,time:2.25)], zone:4),
88 : Station(title: "Woodford", latitude:51.60582, longitude: 0.03328, edg:[(line:2, node:87,time:2.25),(line:2, node:98,time:2.5),(line:2, node:99,time:2.75)], zone:4),
89 : Station(title: "Wanstead", latitude: 51.57565, longitude: 0.02864, edg:[(line:2, node:85,time:2.75),(line:2, node:90,time:2)], zone:4),
90 : Station(title: "Redbridge", latitude: 51.57618, longitude: 0.04542, edg:[(line:2, node:89,time:2),(line:2, node:91,time:1.75)], zone:4),
91 : Station(title: "Gants Hill", latitude: 51.57649, longitude: 0.06637 , edg:[(line:2, node:90,time:1.75),(line:2, node:92,time:3)], zone:4),
92 : Station(title: "Newbury Park", latitude: 51.57565, longitude: 0.08927, edg:[(line:2, node:91,time:3),(line:2, node:93,time:2)], zone:4),
93 : Station(title: "Barkingside", latitude: 51.58543, longitude: 0.08854, edg:[(line:2, node:92,time:2),(line:2, node:94,time:1.75)], zone:4),
94 : Station(title: "Fairlop", latitude: 51.59634, longitude: 0.09125, edg:[(line:2, node:93,time:1.75),(line:2, node:95,time:2)], zone:4),
95 : Station(title: "Hainault", latitude: 51.60288, longitude: 0.09332, edg:[(line:2, node:94,time:2),(line:2, node:96,time:2)], zone:4),
96 : Station(title: "Grange Hill", latitude: 51.61326, longitude: 0.09258, edg:[(line:2, node:95,time:2),(line:2, node:97,time:2)], zone:4),
97 : Station(title: "Chigwell", latitude: 51.61795, longitude: 0.07511, edg:[(line:2, node:96,time:2),(line:2, node:98,time:3.5)], zone:4),
98 : Station(title: "Roding Valley", latitude: 51.61691, longitude: 0.04264, edg:[(line:2, node:88,time:2.5),(line:2, node:97,time:3.5)], zone:4),
99 : Station(title: "Buckhurts Hill", latitude: 51.62611, longitude: 0.04705, edg:[(line:2, node:88,time:2.75),(line:2, node:100,time:2.5)], zone:5),
100 : Station(title: "Loughton", latitude: 51.64170, longitude: 0.05583, edg:[(line:2, node:99,time:2.5),(line:2, node:101,time:3)], zone:6),
101 : Station(title: "Debden", latitude: 51.64535, longitude: 0.08364, edg:[(line:2, node:100,time:3),(line:2, node:102,time:3.5)], zone:6),
102 : Station(title: "Theydon Bois", latitude: 51.67246, longitude: 0.10354, edg:[(line:2, node:101,time:3.5),(line:2, node:103,time:2.75)], zone:6),
103 : Station(title: "Epping", latitude: 51.69365, longitude: 0.11495, edg:[(line:2, node:102,time:2.75)], zone:6),

//Circle (the rest)
104 : Station(title: "Hammersmith", latitude: 51.492328, longitude: -0.222825, edg:[(line:3, node:105,time:1),(line:4, node:117,time:1),(line:4, node:118,time:1),(line:9, node:118,time:1),(line:9, node:115,time:1),(line:5, node:105,time:1)], zone:2),
105 : Station(title: "Goldhawk Road", latitude: 51.501248, longitude: -0.226776, edg:[(line:3, node:104,time:1),(line:3, node:106,time:1),(line:5, node:104,time:1),(line:5, node:106,time:1)], zone:2),
106 : Station(title: "Shepherd's Bush Market", latitude: 51.506063, longitude: -0.226231, edg:[(line:3, node:105,time:1),(line:3, node:107,time:1),(line:5, node:105,time:1),(line:5, node:107,time:1)], zone:2),
107 : Station(title: "Latimer Road", latitude: 51.513125, longitude: -0.218428, edg:[(line:3, node:106,time:1),(line:3, node:108,time:1),(line:5, node:106,time:1),(line:5, node:108,time:1)], zone:2),
108 : Station(title: "Ladbroke Grove", latitude: 51.517300, longitude: -0.211140, edg:[(line:3, node:107,time:1),(line:3, node:190,time:1),(line:5, node:107,time:1),(line:5, node:190,time:1)], zone:2),
190 : Station(title: "Westbourne Park", latitude:51.520995, longitude: -0.201778, edg:[(line:3, node:108,time:1),(line:3, node:191,time:1),(line:5, node:108,time:1),(line:5, node:191,time:1)], zone:2),
191 : Station(title: "Royal Oak", latitude: 51.519383, longitude: -0.187785, edg:[(line:3, node:190,time:1),(line:3, node:61,time:1),(line:5, node:190,time:1),(line:5, node:61,time:1)], zone:2),


//District (the rest)
109 : Station(title: "Ealing Common", latitude: 51.50997, longitude: -0.28825, edg:[(line:4, node:74,time:3),(line:4, node:110,time:3),(line:9, node:110,time:3),(line:9, node:198,time:2.75)], zone:3),
110 : Station(title: "Acton Town", latitude: 51.502500, longitude: -0.278126, edg:[(line:4, node:109,time:3),(line:4, node:111,time:2),(line:9, node:115,time:3),(line:9, node:199,time:4)], zone:3),
111 : Station(title: "Chiswick Park", latitude: 51.49466, longitude: -0.26866,edg:[(line:4, node:110,time:2),(line:4, node:115,time:2)], zone:3),
112 : Station(title: "Gunnersbury", latitude: 51.49126844, longitude: -0.275554312,edg:[(line:4, node:115,time:3.5),(line:4, node:113,time:2.5)], zone:3),
113 : Station(title: "Kew Gardens", latitude: 51.47725641, longitude: -0.284980014, edg:[(line:4, node:112,time:2.5),(line:4, node:114,time:3.5)], zone:3),
114 : Station(title: "Richmond", latitude: 51.46343247, longitude: -0.302033247, edg:[(line:4, node:113,time:3.5)], zone:4),
115 : Station(title: "Turnham Green", latitude: 51.495143, longitude: -0.254060,edg:[(line:4, node:111,time:2),(line:4, node:112,time:3.5),(line:4, node:116,time:1.5),(line:9, node:104,time:5),(line:9, node:110,time:3)], zone:3),
116 : Station(title: "Stamford Brook", latitude: 51.494935, longitude: -0.246190, edg:[(line:4, node:115,time:1.5),(line:4, node:117,time:1.5)], zone:2),
117 : Station(title: "Ravenscourt Park", latitude: 51.494190, longitude: -0.235600, edg:[(line:4, node:116,time:1.5),(line:4, node:104,time:2.5)], zone:2),
118 : Station(title: "Barons Court", latitude: 51.490281, longitude: -0.214340, edg:[(line:4, node:104,time:2),(line:4, node:119,time:1.5),(line:9, node:104,time:2),(line:9, node:120,time:3)], zone:2),
119 : Station(title: "West Kensington", latitude: 51.490850, longitude: -0.205320, edg:[(line:4, node:118,time:1.5),(line:4, node:120,time:3)], zone:2),
120 : Station(title: "Earl's Court", latitude: 51.491521, longitude: -0.193906,edg:[(line:4, node:119,time:3),(line:4, node:121,time:3.5),(line:4, node:122,time:2),(line:4, node:2,time:3.5),(line:4, node:3,time:2.5)], zone:2),
121 : Station(title: "Kensington(Olympia)", latitude: 51.49675, longitude: -0.20931, edg:[(line:4, node:120,time:3.5)], zone:2),
122 : Station(title: "West Brompton", latitude: 51.48681, longitude: -0.19475, edg:[(line:4, node:120,time:2),(line:4, node:123,time:2)], zone:2),
123 : Station(title: "Fulham Broadway",latitude: 51.48033 ,longitude: -0.19488, edg:[(line:4, node:122,time:2),(line:4, node:124,time:2)], zone:2),
124 : Station(title: "Parsons Green", latitude: 51.47488, longitude: -0.20205, edg:[(line:4, node:123,time:2),(line:4, node:125,time:2)], zone:2),
125 : Station(title: "Putney Bridge", latitude: 51.46868, longitude: -0.2088141, edg:[(line:4, node:124,time:2),(line:4, node:126,time:2.5)], zone:2),
126 : Station(title: "East Putney", latitude: 51.459015, longitude: -0.211138  ,edg:[(line:4, node:125,time:2.5),(line:4, node:127,time:2.5)], zone:3),
127 : Station(title: "Southfields", latitude: 51.444941, longitude: -0.206466,edg:[(line:4, node:126,time:2.5),(line:4, node:128,time:2.5)], zone:3),
128 : Station(title: "Wimbledon Park", latitude: 51.43391, longitude: -0.19864,edg:[(line:4, node:127,time:2.5),(line:4, node:129,time:2.5)], zone:3),
129 : Station(title: "Wimbledon", latitude: 51.42200, longitude: -0.20544,edg:[(line:4, node:128,time:2.5)], zone:3),
//'''bottle'''
130 : Station(title: "Aldgate East", latitude: 51.51514, longitude: -0.07178, edg:[(line:4, node:31,time:2.5),(line:4, node:131,time:2),(line:5, node:30,time:2.5),(line:5, node:131,time:2)], zone:1),
131 : Station(title: "Whitechapel", latitude: 51.51946, longitude: -0.05951, edg:[(line:4, node:130,time:2),(line:4, node:132,time:2),(line:5, node:130,time:2),(line:5, node:132,time:2)], zone:2),
132 : Station(title: "Stepney Green", latitude: 51.521884, longitude: -0.046546, edg:[(line:4, node:131,time:2),(line:4, node:82,time:2),(line:5, node:82,time:2)], zone:2),
133 : Station(title: "Bow Church", latitude: 51.5289, longitude: -0.0210,edg:[(line:4, node:82,time:1.5),(line:5, node:82,time:1.5),(line:4, node:134,time:1.5),(line:5, node:134,time:1.5)], zone:2),
134 : Station(title: "Bow Road", latitude: 51.52709, longitude: -0.02482, edg:[(line:4, node:82,time:1.5),(line:4, node:135,time:2),(line:5, node:82,time:1.5),(line:5, node:135,time:2)], zone:2),
135 : Station(title: "Bromley-by-Bow", latitude: 51.52466, longitude: -0.01282,edg:[(line:4, node:134,time:2),(line:4, node:136,time:2.5),(line:5, node:134,time:2),(line:5, node:136,time:2.5)], zone:2),
136 : Station(title: "West Ham", latitude: 51.527917, longitude: 0.004141 ,edg:[(line:4, node:135,time:2.5),(line:4, node:137,time:1.5),(line:5, node:135,time:2.5),(line:5, node:137,time:1.5),(line:6, node:83,time:2.75),(line:6, node:167,time:1)], zone:3),
137 : Station(title: "Plaistow",latitude: 51.53122 , longitude:0.01659,edg:[(line:4, node:136,time:1.5),(line:4, node:267,time:2),(line:5, node:136,time:1.5),(line:5, node:267,time:2)], zone:3),
138 : Station(title: "East Ham", latitude: 51.53931, longitude: 0.05260,edg:[(line:4, node:267,time:2.5),(line:4, node:139,time:3.5),(line:5, node:267,time:2.5),(line:5, node:139,time:3.5)], zone:3),
139 : Station(title: "Barking",latitude: 51.54079, longitude: 0.08018,edg:[(line:4, node:138,time:3.5),(line:4, node:140,time:2),(line:5, node:138,time:3.5)], zone:4),
140 : Station(title: "Upney", latitude: 51.53824, longitude: 0.09998,edg:[(line:4, node:139,time:2),(line:4, node:141,time:2.5)], zone:4),
141 : Station(title: "Becontree", latitude: 51.54029, longitude: 0.12620, edg:[(line:4, node:140,time:2.5),(line:4, node:142,time:2.5)], zone:5),
142 : Station(title: "Dagenham Heathway", latitude: 51.54173, longitude: 0.14543,edg:[(line:4, node:141,time:2.5),(line:4, node:143,time:2)], zone:5),
143 : Station(title: "Dagenham Wast", latitude: 51.54396, longitude: 0.16439, edg:[(line:4, node:142,time:2),(line:4, node:144,time:3)], zone:5),
144 : Station(title: "Elm Park", latitude: 51.54969, longitude: 0.19779,edg:[(line:4, node:143,time:3),(line:4, node:145,time:2.5)], zone:6),
145 : Station(title: "Hornchurch", latitude: 51.55387, longitude: 0.21797,edg:[(line:4, node:144,time:2.5),(line:4, node:146,time:2)], zone:6),
146 : Station(title: "Upmister Bridge", latitude: 51.55847, longitude: 0.23509,edg:[(line:4, node:145,time:2),(line:4, node:147,time:2)], zone:6),
147 : Station(title: "Upminster", latitude: 51.55925, longitude: 0.25177,edg:[(line:4, node:146,time:2)], zone:6),

267 : Station(title: "Upton Park", latitude: 51.53514, longitude: 0.03373, edg:[(line:4, node:137,time:2),(line:4, node:138,time:2.5),(line:5, node:137,time:2),(line:5, node:138,time:2.5)], zone:3),

//H&C
//'''intersected'''

//Jubilee
148 : Station(title: "Stanmore", latitude: 51.61895, longitude: -0.30241,edg:[(line:6 ,node:149,time:2)], zone:5),
149 : Station(title: "Canons Park",latitude: 51.60810, longitude: -0.29487, edg:[(line:6 ,node:148,time:2),(line:6 ,node:150,time:2.25)], zone:5),
150 : Station(title: "Queensbury",latitude: 51.59437, longitude: -0.28655,edg:[(line:6 ,node:149,time:2.25),(line:6 ,node:151,time:2)], zone:4),
151 : Station(title: "Kingsbury", latitude: 51.58422, longitude: -0.27880,edg:[(line:6 ,node:150,time:2),(line:6 ,node:152,time:3.5)], zone:4),
152 : Station(title: "Wembley Park", latitude:51.56355598, longitude: -0.279770242,edg:[(line:6 ,node:151,time:3.5),(line:6 ,node:153,time:3),(line:7 ,node:189,time:2.5),(line:7 ,node:158,time:7)], zone:4),
153 : Station(title: "Neasden",latitude: 51.55427, longitude: -0.25042,edg:[(line:6 ,node:152,time:3),(line:6 ,node:154,time:1.75)], zone:3),
154 : Station(title: "Dollis Hill",latitude:51.55250, longitude: -0.23884,edg:[(line:6 ,node:153,time:1.75),(line:6 ,node:155,time:1.75)], zone:3),
155 : Station(title: "Willesden Green", latitude: 51.54919 , longitude: -0.22106,edg:[(line:6 ,node:154,time:1.75),(line:6 ,node:156,time:2.25)], zone:3),
156 : Station(title: "Kilburn", latitude: 51.5471715, longitude: -0.2050020, edg:[(line:6 ,node:155,time:2.25),(line:6 ,node:157,time:1.75)], zone:2),
157 : Station(title: "West Hampstead", latitude: 51.54678865, longitude: -0.191016868,edg:[(line:6 ,node:156,time:1.75),(line:6 ,node:158,time:1.5)], zone:2),
158 : Station(title: "Finchley Road",latitude: 51.546701, longitude: -0.179867,edg:[(line:6 ,node:157,time:1.5),(line:6 ,node:159,time:1.75),(line:7 ,node:152,time:7),(line:7 ,node:24,time:5.5)], zone:2),
159 : Station(title: "Swiss Cottage",latitude: 51.54333, longitude: -0.17436,edg:[(line:6 ,node:158,time:1.75),(line:6 ,node:160,time:1.75)], zone:2),
160 : Station(title: "St.John's Wood", latitude: 51.53472 , longitude: -0.17428,edg:[(line:6 ,node:159,time:1.75),(line:6 ,node:24,time:2.75)], zone:2),
//'''bottle'''
161 : Station(title: "Southwark", latitude: 51.50384, longitude: -0.10478 ,edg:[(line:6 ,node:64,time:1.25),(line:6 ,node:162,time:1.75)], zone:1),
162 : Station(title: "London Bridge", latitude: 51.505353, longitude: -0.084826,edg:[(line:6 ,node:161,time:1.75),(line:6 ,node:163,time:2.25),(line:8,node:246,time:1.5),(line:8 ,node:32,time:2)], zone:1),
163 : Station(title: "Bermondsey", latitude: 51.49813, longitude: -0.06350,edg:[(line:6 ,node:162,time:2.25),(line:6 ,node:164,time:1.75)], zone:2),
164 : Station(title: "Canada Water", latitude: 51.49787 , longitude: -0.04967,edg:[(line:6 ,node:163,time:1.75),(line:6 ,node:165,time:2.5)], zone:2),
165 : Station(title: "Canary Wharf",latitude: 51.50362 , longitude: -0.01987,edg:[(line:6 ,node:164,time:2.5),(line:6 ,node:166,time:2)], zone:2),
166 : Station(title: "North Greenwich", latitude: 51.50056, longitude: 0.00360,edg:[(line:6 ,node:165,time:2),(line:6 ,node:167,time:2.5)], zone:2),
167 : Station(title: "Canning Town",latitude: 51.513880, longitude: 0.008876 ,edg:[(line:6 ,node:166,time:2.5),(line:6 ,node:136,time:2)], zone:3),

//Metropolitan
168 : Station(title: "Amersham", latitude: 51.67435, longitude: -0.60732 ,edg:[(line:7 ,node:170,time:4)], zone:9),
169 : Station(title: "Chesham", latitude: 51.70522733, longitude: -0.610986426, edg:[(line:7 ,node:170,time:8.5)], zone:9),
170 : Station(title: "Chalfont & Latimer", latitude: 51.66822, longitude: -0.56053,edg:[(line:7 ,node:168,time:4),(line:7 ,node:169,time:8.5),(line:7 ,node:171,time:4)], zone:8),
171 : Station(title: "Chorleywood", latitude: 51.65429872, longitude: -0.518373353, edg:[(line:7 ,node:170,time:4),(line:7 ,node:172,time:4.5)], zone:7),
172 : Station(title: "Rickmansworth",latitude: 51.64027898, longitude: -0.473521454, edg:[(line:7 ,node:171,time:4.5),(line:7 ,node:175,time:4)], zone:7),
173 : Station(title: "Croxley", latitude: 51.64733268, longitude: -0.441479115, edg:[(line:7 ,node:174,time:3.5),(line:7 ,node:175,time:4)], zone:7),
174 : Station(title: "Watford", latitude: 51.65741058, longitude: -0.41745306, edg:[(line:7 ,node:173,time:3.5)], zone:7),
175 : Station(title: "Moor Park", latitude: 51.62970512, longitude: -0.432070848, edg:[(line:7 ,node:172,time:4),(line:7 ,node:173,time:4),(line:7 ,node:176,time:3)], zone:7),
176 : Station(title: "Northwood", latitude: 51.61098984, longitude: -0.423584422,edg:[(line:7 ,node:175,time:3),(line:7 ,node:177,time:2.5)], zone:6),
177 : Station(title: "Northwood Hills", latitude: 51.60043795, longitude: -0.408812575,edg:[(line:7 ,node:176,time:2.5),(line:7 ,node:178,time:3)], zone:6),
178 : Station(title: "Pinner", latitude: 51.59270271, longitude: -0.380539158, edg:[(line:7 ,node:177,time:3),(line:7 ,node:179,time:2)], zone:5),
179 : Station(title: "North Harrow", latitude: 51.58503612, longitude: -0.362595264,edg:[(line:7 ,node:178,time:2),(line:7 ,node:180,time:3)], zone:5),
180 : Station(title: "Harrow-on-the-Hill", latitude: 51.579335, longitude: -0.337052,edg:[(line:7 ,node:179,time:3),(line:7 ,node:187,time:2),(line:7 ,node:188,time:2.5)], zone:5),
181 : Station(title: "Uxbridge", latitude: 51.54687, longitude: -0.47725,edg:[(line:7 ,node:182,time:3.5),(line:9 ,node:182,time:3.5)], zone:6),
182 : Station(title: "Hillingdon", latitude: 51.55364, longitude: -0.45062,edg:[(line:7 ,node:181,time:3.5),(line:9 ,node:181,time:3.5),(line:7 ,node:183,time:2),(line:9 ,node:183,time:2)], zone:6),
183 : Station(title: "Ickenham", latitude: 51.56147, longitude: -0.44257,edg:[(line:7 ,node:182,time:2),(line:9 ,node:182,time:2),(line:7 ,node:184,time:2.5),(line:9 ,node:184,time:2.5)], zone:6),
184 : Station(title: "Ruislip", latitude:51.57146, longitude: -0.42157,edg:[(line:7 ,node:183,time:2.5),(line:9 ,node:183,time:2.5),(line:7 ,node:185,time:1.5),(line:9 ,node:185,time:1.5)], zone:6),
185 : Station(title: "Ruislip Manor",latitude: 51.57349 , longitude: -0.41185,edg:[(line:7 ,node:184,time:1.5),(line:9 ,node:184,time:1.5),(line:7 ,node:186,time:2),(line:9 ,node:186,time:2)], zone:6),
186 : Station(title: "Eastcote", latitude: 51.57676, longitude: -0.39629,edg:[(line:7 ,node:185,time:2),(line:7 ,node:192,time:2.5),(line:9 ,node:192,time:2.5),(line:9 ,node:185,time:2)], zone:5),
187 : Station(title: "West Harrow", latitude: 51.5795432, longitude: -0.353144584,edg:[(line:7 ,node:192,time:2.5),(line:7 ,node:180,time:2)], zone:5),
188 : Station(title: "Northwick Park", latitude: 51.57850117 , longitude: -0.317857023, edg:[(line:7 ,node:180,time:2.5),(line:7 ,node:189,time:2.5)], zone:4),
189 : Station(title: "Preston Road", latitude: 51.57229051, longitude: -0.295813875,edg:[(line:7 ,node:188,time:2.5),(line:7 ,node:152,time:2.5)], zone:4),

//Northern
222 : Station(title: "Edgware", latitude: 51.61385,longitude: -0.27515,edg:[(line: 8, node: 223, time: 2.5)], zone: 5),
223 : Station(title: "Burnt Oak", latitude: 51.60300,longitude: -0.26427,edg:[(line: 8, node: 222, time: 2.5), (line: 8, node: 224, time: 2.5)], zone: 4),
224 : Station(title: "Colindale", latitude: 51.59556,longitude: -0.24996,edg:[(line: 8, node: 223, time: 2.5), (line: 8, node: 225, time: 3)], zone: 4),
225 : Station(title: "Hendon Central", latitude: 51.58346,longitude: -0.22653,edg:[(line: 8, node: 224, time: 3), (line: 8, node: 226, time: 2)], zone: 4),
226 : Station(title: "Brent Cross", latitude: 51.57693,longitude: -0.21414,edg:[(line: 8, node: 225, time: 2), (line: 8, node: 227, time: 3)], zone: 3),
227 : Station(title: "Golders Green", latitude: 51.57249,longitude: -0.19410,edg:[(line: 8, node: 226, time: 3), (line: 8, node: 228, time: 3.5)], zone: 3),
228 : Station(title: "Hampstead", latitude: 51.55645,longitude: -0.17851,edg:[(line: 8, node: 227, time: 3.5), (line: 8, node: 229, time: 2)], zone: 3),
229 : Station(title: "Belsize Park", latitude: 51.55036,longitude: -0.16465,edg:[(line: 8, node: 228, time: 2), (line: 8, node: 230, time: 2)], zone: 2),
230 : Station(title: "Chalk Farm", latitude: 51.54390,longitude: -0.15384,edg:[(line: 8, node: 229, time: 2), (line: 8, node: 231, time: 2)], zone: 2),
231 : Station(title: "Camden Town", latitude: 51.54192,longitude: -0.13981,edg:[(line: 8, node: 230, time: 2), (line: 8, node: 232, time: 2),(line: 8, node: 233, time: 2.5)], zone: 2),
232 : Station(title: "Mornington Crescent", latitude: 51.53427,longitude: -0.13901,edg:[(line: 8, node: 231, time: 1.5), (line: 8, node: 266, time: 2)], zone: 2),
233 : Station(title: "Kentish Town", latitude: 51.55046 ,longitude:-0.14067 ,edg:[(line: 8, node: 231, time: 2.5), (line: 8, node: 234, time: 1.5)], zone: 2),
234 : Station(title: "Tufnell Park", latitude: 51.55663,longitude:-0.13813 ,edg:[(line: 8, node: 235, time: 1.5), (line: 8, node: 233, time: 1.5)], zone: 2),
235 : Station(title: "Archway", latitude:51.56536  ,longitude: -0.13474,edg:[(line: 8, node: 234, time: 1.5), (line: 8, node: 236, time: 3)], zone: 2),
236 : Station(title: "Highgate", latitude: 51.57722,longitude:-0.14553 ,edg:[(line: 8, node: 235, time: 3), (line: 8, node: 237, time: 3)], zone: 3),
237 : Station(title: "East Finchley", latitude: 51.5872974705487,longitude: -0.16495632312818,edg:[(line: 8, node: 236, time: 3), (line: 8, node: 238, time: 3.5)], zone: 3),
238 : Station(title: "Finchley Central", latitude: 51.60102 ,longitude: -0.19240,edg:[(line: 8, node: 237, time: 3.5), (line: 8, node: 239, time: 3),(line: 8, node: 240, time: 2.5)], zone: 4),
239 : Station(title: "Mill Hill East", latitude:51.60892 ,longitude: -0.20965,edg:[(line: 8, node: 238, time: 3)], zone: 4),
240 : Station(title: "West Finchley", latitude:51.60922281219 ,longitude: -0.188711152265457,edg:[(line: 8, node: 238, time: 2.5), (line: 8, node: 241, time: 1.5)], zone: 4),
241 : Station(title: "Woodside Park", latitude: 51.6181717295887,longitude: -0.185578887883903,edg:[(line: 8, node: 240, time: 1.5), (line: 8, node: 242, time: 2.5)], zone: 4),
242 : Station(title: "Totteridge & Whetstone", latitude:51.63022 ,longitude:-0.17917 ,edg:[(line: 8, node: 241, time: 2.5), (line: 8, node: 243, time: 3.5)], zone: 4),
243 : Station(title: "High Barnet", latitude: 51.6506519031751,longitude:-0.194138165994553 ,edg:[(line: 8, node: 242, time: 3.5)], zone: 5),
244 : Station(title: "Angel", latitude: 51.53253,longitude: -0.10579,edg:[(line: 8, node: 47, time: 2.5), (line: 8, node: 245, time: 3)], zone: 1),
245 : Station(title: "Old Street", latitude: 51.52618,longitude: -0.08751,edg:[(line: 8, node: 244, time: 3), (line: 8, node: 29, time: 1.5)], zone: 1),
246 : Station(title: "Borough", latitude: 51.50095,longitude: -0.09446,edg:[(line: 8, node: 162, time: 1.5), (line: 8, node: 66, time: 2)], zone: 1),
247 : Station(title: "Kennington", latitude: 51.48823,longitude: -0.10587,edg:[(line: 8, node: 66, time: 2.5), (line: 8, node: 248, time: 2),(line: 8, node: 64, time: 3.5)], zone: 2),
248 : Station(title: "Oval", latitude: 51.48214,longitude: -0.11284,edg:[(line: 8, node: 247, time: 2), (line: 8, node: 249, time: 2.5)], zone: 2),
249 : Station(title: "Stockwell", latitude: 51.47224,longitude: -0.12270,edg:[(line: 8, node: 248, time: 2.5), (line: 8, node: 250, time: 1.5), (line: 10, node: 268, time: 5),(line: 10, node: 259, time: 2.25)], zone: 2),
250 : Station(title: "Clapham North", latitude: 51.46522,longitude: -0.12950,edg:[(line: 8, node: 249, time: 1.5), (line: 8, node: 251, time: 1.5)], zone: 2),
251 : Station(title: "Clapham Common", latitude: 51.46156,longitude: -0.13802,edg:[(line: 8, node: 250, time: 1.5), (line: 8, node: 252, time: 2.5)], zone: 2),
252 : Station(title: "Clapham South", latitude: 51.45292257,longitude: -0.147562148,edg:[(line: 8, node: 251, time: 2.5), (line: 8, node: 253, time: 2)], zone: 2),
253 : Station(title: "Balham", latitude: 51.44344113,longitude: -0.152945844,edg:[(line: 8, node: 252, time: 2), (line: 8, node: 254, time: 2)], zone: 3),
254 : Station(title: "Tooting Bec", latitude: 51.43578971,longitude: -0.159635693,edg:[(line: 8, node: 253, time: 2), (line: 8, node: 255, time: 2)], zone: 3),
255 : Station(title: "Tooting Broadway", latitude: 51.42780008,longitude: -0.167910811,edg:[(line: 8, node: 254, time: 2), (line: 8, node: 256, time: 2)], zone: 3),
256 : Station(title: "Colliers Wood", latitude: 51.41823649,longitude: -0.17800983,edg:[(line: 8, node: 255, time: 2), (line: 8, node: 257, time: 2)], zone: 3),
257 : Station(title: "South Wimbledon", latitude: 51.41543048,longitude: -0.192262754,edg:[(line: 8, node: 256, time: 2), (line: 8, node: 258, time: 3.5)], zone: 3),
258 : Station(title: "Morden", latitude: 51.40222413,longitude: -0.194599126,edg:[(line: 8, node: 257, time: 3.5)], zone: 4),

266 : Station(title: "Euston", latitude: 51.52774, longitude: -0.13303, edg:[(line: 8, node: 232, time: 2), (line: 8, node: 41, time: 1.5),(line: 8, node: 47, time: 1.75)], zone: 1),


//Picadiily
192 : Station(title: "Rayners Lane", latitude: 51.57534, longitude: -0.37240, edg:[(line:9 ,node:186 ,time:2.5),(line:9 ,node:193 ,time:3),(line:7 ,node:186 ,time:2.5),(line:7,node:187 ,time:2.5)], zone: 5),
193 : Station(title: "South Harrow", latitude: 51.56461, longitude: -0.35210, edg:[(line:9 ,node: 192,time:3),(line: 9,node:194 ,time:2.5)], zone: 5),
194 : Station(title: "Sudbury Hill", latitude: 51.556998,longitude: -0.335949,edg:[(line:9 ,node:193 ,time:2.5),(line: 9,node: 195,time:2.5)], zone: 4),
195 : Station(title: "Sudbury Town", latitude: 51.55093,longitude: -0.31598,edg:[(line:9 ,node:194 ,time:2.5),(line: 9,node: 196,time:2.5)], zone: 4),
196 : Station(title: "Alperton", latitude: 51.54097,longitude: -0.30061,edg:[(line:9 ,node:195 ,time:2.5),(line: 9,node: 197,time:2.5)], zone: 4),
197 : Station(title: "Park Royal", latitude: 51.52671,longitude: -0.28417,edg:[(line:9 ,node:196 ,time:2.5),(line: 9,node: 198,time:2)], zone: 3),
198 : Station(title: "North Ealing", latitude: 51.51807,longitude: -0.28838,edg:[(line:9 ,node:197 ,time:2),(line: 9,node: 109,time:2)], zone: 3),
199 : Station(title: "South Ealing", latitude: 51.500801,longitude: -0.307856,edg:[(line:9 ,node:110 ,time:4),(line: 9,node: 200,time:1)], zone: 3),
200 : Station(title: "Northfeilds", latitude: 51.499856,longitude: -0.313188,edg:[(line:9 ,node:199 ,time:1),(line: 9,node: 201,time:2.5)], zone: 3),
201 : Station(title: "Boston Manor", latitude: 51.495371,longitude: -0.325573,edg:[(line:9 ,node:200 ,time:2.5),(line: 9,node: 202,time:3.5)], zone: 4),
202 : Station(title: "Osterly", latitude: 51.481569,longitude: -0.351960,edg:[(line:9 ,node:201 ,time:3.5),(line: 9,node: 203,time:2)], zone: 4),
203 : Station(title: "Hounslow East", latitude: 51.473540,longitude: -0.356000,edg:[(line:9 ,node:202 ,time:2),(line: 9,node: 204,time:1.5)], zone: 4),
204 : Station(title: "Hounslow Central", latitude: 51.471290,longitude: -0.365853,edg:[(line:9 ,node:203 ,time:1.5),(line: 9,node: 205,time:2.5)], zone: 4),
205 : Station(title: "Hounslow West", latitude: 51.473689,longitude: -0.386471,edg:[(line:9 ,node:204 ,time:2.5),(line: 9,node: 206,time:3.5)], zone: 5),
206 : Station(title: "Hatton Cross", latitude: 51.466988,longitude: -0.423035,edg:[(line:9 ,node:205 ,time:3.5),(line: 9,node: 207,time:2.5),(line: 9,node: 208,time:3)], zone: 6),
207 : Station(title: "Heathrow Terminal 4", latitude: 51.4594777,longitude: -0.44689658,edg:[(line:9 ,node:206 ,time:2.5),(line: 9,node: 208,time:6)], zone: 6),
208 : Station(title: "Heathrow Terminal 1-2-3", latitude: 51.4712899,longitude: -0.4527438,edg:[(line:9 ,node:207 ,time:6),(line: 9,node: 206,time:3)], zone: 6),
//209 : Station(title: "Heathrow Terminal 5", latitude: ,longitude: ,edg:[]),

210 : Station(title: "Caledonian Road", latitude: 51.54850 ,longitude: -0.11802,edg:[(line:9 ,node:211 ,time:1.5),(line: 9,node: 47,time:3)], zone: 2),
211 : Station(title: "Holloway Road", latitude: 51.55290,longitude: -0.11278,edg:[(line:9 ,node:210 ,time:1.5),(line: 9,node: 212,time:1.5)], zone: 2),
212 : Station(title: "Arsenal", latitude: 51.55847,longitude: -0.10561,edg:[(line:9 ,node:211 ,time:1.5),(line: 9,node: 213,time:1.5)], zone: 2),
213 : Station(title: "Finsbury Park", latitude: 51.56498,longitude: -0.10543,edg:[(line:9 ,node:212 ,time:1.5),(line: 9,node: 214,time:2),(line:10 ,node:261 ,time:2.5),(line: 10,node: 262,time:3.5)], zone: 2),
214 : Station(title: "Manor House", latitude: 51.57038,longitude: -0.09601,edg:[(line:9 ,node:215 ,time:3.5),(line: 9,node: 213,time:2)], zone: 2),
215 : Station(title: "Turnpike Lane", latitude: 51.59031,longitude: -0.10308,edg:[(line:9 ,node:214 ,time:3.5),(line: 9,node: 216,time:2)], zone: 3),
216 : Station(title: "Wood Green", latitude: 51.59709,longitude: -0.10939,edg:[(line:9 ,node:215 ,time:2),(line: 9,node: 217,time:2.5)], zone: 3),
217 : Station(title: "Bounds Green", latitude: 51.60700,longitude: -0.12418,edg:[(line:9 ,node:216 ,time:2.5),(line: 9,node: 218,time:3.5)], zone: 3),
218 : Station(title: "Arnos Grove", latitude: 51.61625,longitude: -0.13355,edg:[(line:9 ,node:217 ,time:3.5),(line: 9,node: 219,time:3)], zone: 4),
219 : Station(title: "Southgate", latitude: 51.63240,longitude: -0.12765,edg:[(line:9 ,node:218 ,time:3),(line: 9,node: 220,time:3)], zone: 4),
220 : Station(title: "Oakwood", latitude: 51.64739,longitude: -0.13192,edg:[(line:9 ,node:219 ,time:3),(line: 9,node: 221,time:3)], zone: 5),
221 : Station(title: "Cockfosters", latitude: 51.65117,longitude: -0.14811,edg:[(line: 9,node: 220,time:3)], zone: 5),

//Victoria
259 : Station(title: "Vauxhall", latitude: 51.48603,longitude:-0.12369, edg:[(line: 10, node: 260, time: 1.5), (line: 10, node: 249, time: 2.25)], zone: 1),
260 : Station(title: "Pimlico", latitude: 51.489081,longitude: -0.133037, edg:[(line: 10, node: 259, time: 1.5), (line: 10, node: 6, time: 2.5)], zone: 1),
261 : Station(title: "Highbury & Islington", latitude: 51.54502,longitude:-0.10752, edg:[(line: 10, node: 213, time: 2.5), (line: 10, node: 47, time: 2.75)], zone: 2),
262 : Station(title: "Seven Sisters", latitude: 51.58318,longitude:-0.07495 , edg:[(line: 10, node: 263, time: 1.75), (line: 10, node: 213, time: 3.5)], zone: 3),
263 : Station(title: "Tottenham Hale", latitude: 51.588878,longitude:-0.059808, edg:[(line: 10, node: 262, time: 1.75), (line: 10, node: 264, time: 2)], zone: 3),
264 : Station(title: "Blackhorse Road", latitude: 51.58698,longitude:-0.04104, edg:[(line: 10, node: 265, time: 2), (line: 10, node: 263, time: 2)], zone: 3),
265 : Station(title: "Walthamstow Central", latitude: 51.58293  ,longitude:-0.019966, edg:[(line: 10, node: 264, time: 2)], zone: 3),

268 : Station(title: "Brixton", latitude: 51.46239 , longitude: -0.115421, edg:[(line: 10, node: 249, time: 2)], zone: 2)
]