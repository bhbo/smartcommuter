//
//  MinDistanceSearch.swift
//  MapViewDemo
//
//  Created by Bo Watthanavarangkul on 3/6/2559 BE.
//  Copyright © 2559 Ravi Shankar. All rights reserved.
//

import Foundation

class MinDistanceSearch{
    var node_table : Dictionary<Int,Station>
    var line_table : Dictionary<Int,Line>
    
    init(node_table:Dictionary<Int,Station>,line_table:Dictionary<Int,Line>){
        self.node_table=node_table
        self.line_table=line_table
    }
    
    func findNodeWithMinCost(nodeList:[ANode]) ->(ANode,Int){
        /*
         var nodeWithMinCost = nodeList[0]      
         var minIndex=0;
         
         for (var i=1;i<nodeList.count;i++){
         if nodeWithMinCost.getEvalCost() > nodeList[i].getEvalCost(){
         nodeWithMinCost = nodeList[i]
         minIndex = i
         }
         }
         return (nodeWithMinCost, minIndex)}*/
        
        
        let nodeWithMinCost = nodeList.enumerate().minElement( { $0.1.getEvalCost() < $1.1.getEvalCost()} )!
        return (nodeWithMinCost.element, nodeWithMinCost.index)}
    
    
    
    func findPath(var node : ANode) ->[(line:Int?,node:Int,time:Double)]{
        //return path from start to goal #node:Node()
        var path:[(line:Int?,node:Int,time:Double)]=[]
        path.insert((line:node.getLine_id(), node:node.getTo_node_id(),time:node.time),atIndex:0)
        while (node.hasParentNode()){
            path.insert((line:node.getParentNode().getLine_id(), node:node.getParentNode().getTo_node_id(), time:node.getParentNode().time),atIndex: 0)
            node = node.getParentNode()
        }
        
        return path     //[(line_id, to_node_id), ...]
        
    }
    
    
    func calShortestPathHCost(startId:Int,goalId:Int)-> Double{
        
        let lat1=self.node_table[startId]!.latitude
        let lon1=self.node_table[startId]!.longitude
        let lat2=self.node_table[goalId]!.latitude
        let lon2=self.node_table[goalId]!.longitude
        
        let lonA = Double(lon1)*(M_PI/180)
        let lonB = Double(lon2)*(M_PI/180)
        let latA = Double(lat1)*(M_PI/180)
        let latB = Double(lat2)*(M_PI/180)
        
        
        // haversine formula
        
        let dlon = lonB - lonA
        let dlat = latB - latA
        let a = pow(sin(dlat/2),2.0) + cos(latA) * cos(latB) * pow(sin(dlon/2),2)
        let c = 2 * asin(sqrt(a))
        let r = 6371.0
        return c*r
        
    }
    
    func calShortestPathGCost(currentPath:[(line:Int?,node:Int,time:Double)])->Double{
        if noOfCal == 1{
            var distance:Int = 0
            for (var i = 2;i < currentPath.count;i++){
                if currentPath[i].line != currentPath[i-1].line{
                    distance+=3
                }
            }
            return Double(distance)
        }
            
            //Min-Distance
        else if noOfCal == 2{
            var distance:Double = 0.0
            for (var i = 1;i < currentPath.count;i++) {
                //line = self.line_table[ currentPath[i][0] ]
                //length = line[1]
                var length = self.calShortestPathHCost(currentPath[i-1].node,goalId: currentPath[i].node)
                distance += length
            }
            return distance
        }
            
        else if noOfCal == 3{
            var time = 0.0
            for (var i = 1;i < currentPath.count;i++){
                time+=currentPath[i].time
                
            }
            return time
        }
        
        return 0.0

    }
    
    
    func callShortestPathGCost(currentPath:[(line:Int?,node:Int,time:Double)])->Int{
        //Min-Transfer
        return 1

    }
    
    
    func shortestPathSearch(start_id: Int, goal_id: Int)->[(line:Int?,node:Int,time:Double)]{
        var queue : Array<ANode> = []
        var gCost = 0.0
        var hCost:Double = calShortestPathHCost(start_id,goalId: goal_id)
        queue.append(ANode(line_id: nil, to_node_id: start_id, time:0.0, parentNode: nil, gCost: gCost, hCost: hCost))
        var closedList : Array<Int> = []
        var found : Bool = false
        var currentPath:[(line:Int?,node:Int,time:Double)]=[]
        var currentNode : ANode
        var minIndex : Int
        (currentNode,minIndex) = self.findNodeWithMinCost(queue)
        while (queue.count > 0){
            (currentNode,minIndex) = self.findNodeWithMinCost(queue)
            queue.removeAtIndex(minIndex)
            closedList.append(currentNode.getTo_node_id())
            currentPath = findPath(currentNode)
            if (currentNode.getTo_node_id() == goal_id){
                found = true
                break
            }
            else{
                var adjIdList :Array<(line:Int?,node:Int,time:Double)> = []
                adjIdList = self.node_table[currentNode.getTo_node_id()]!.edges
                for(var i = 0; i < adjIdList.count; i++){
                    if !(closedList.contains(adjIdList[i].node)){
                        gCost = calShortestPathGCost( currentPath + [adjIdList[i]] )
                        hCost = calShortestPathHCost( adjIdList[i].node, goalId: goal_id )
                        
                        queue.append(ANode(line_id: adjIdList[i].line, to_node_id: adjIdList[i].node,time: adjIdList[i].time, parentNode: currentNode, gCost: gCost, hCost: hCost))
                        
                    }
                }
            }
        }
        
        
        
        if !found{
            print ("Empty Queue!:shortestPath")
            return []
        }
        else{
            let walk = findPath(currentNode)
            return walk
        }
    }
}

