//
//  Station.swift
//  MapViewDemo
//
//  Created by Bo Watthanavarangkul on 3/6/2559 BE.
//  Copyright © 2559 Ravi Shankar. All rights reserved.
//

import Foundation
import MapKit

class Station :Place{
    
    var edges : [(line:Int?,node:Int,time: Double)]
    var zone : Int
    
    
    init(title: String,latitude: Double, longitude: Double, edg:[(line:Int?,node:Int,time: Double)],zone:Int) {
        
        self.edges=edg
        self.zone = zone
        super.init(title: title,latitude: latitude,longitude: longitude)
        super.type = "Station"
        
    }
    
    
}