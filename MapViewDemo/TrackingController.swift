//
//  TrackingController.swift
//  MapViewDemo
//
//  Created by Bo Watthanavarangkul on 5/16/2559 BE.
//  Copyright © 2559 Ravi Shankar. All rights reserved.
//
import UIKit
import MapKit
import CoreLocation




class TrackingController: UIViewController,MKMapViewDelegate, CLLocationManagerDelegate{
    
    
    //var titleString: String!
    //var selectedPath : [(line:Int?,node:Int,time:Double)]!
    
    
    var drawRoute = 1
    var locationManager: CLLocationManager!
    
    
    @IBOutlet weak var mapView: MKMapView!
    
    //@IBOutlet weak var someLabel: UILabel!
    //@IBOutlet weak var titleLabel: UILabel!
    
    weak var delegate: HandleSelectedPath?
   
    
    
   /* @IBAction func stopTrackButtonAction(sender: AnyObject) {
        
        locationManager.stopUpdatingHeading()
        locationManager.stopMonitoringSignificantLocationChanges()
        viewWillDisappear(true)
        mapView.showsUserLocation = false
        
        delegate?.selectedPathDraw([(line:1,node:1,time:1.0)])
        dismissViewControllerAnimated(true, completion: nil)
        definesPresentationContext = true
    }*/
    
    @IBAction func stopTrackButton(sender: AnyObject) {
        
        locationManager.stopUpdatingHeading()
        locationManager.stopMonitoringSignificantLocationChanges()
        viewWillDisappear(true)
        mapView.showsUserLocation = false
        
        delegate?.selectedPathDraw([(line:1,node:1,time:1.0)])
        dismissViewControllerAnimated(true, completion: nil)
        definesPresentationContext = true
        
    }
    override func viewDidLoad()
    {
        super.viewDidLoad()
        //someLabel.text="eiei"
        
        self.navigationItem.title = "\(stations[selectedPath![0].node]!.title) to \(stations[selectedPath![(selectedPath!.count)-1].node]!.title)"
        
        //zoomToRegion()
        //routeDraw()
        
        locationManager = CLLocationManager()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        locationManager.delegate = self;
        
        // user activated automatic authorization info mode
        let status = CLLocationManager.authorizationStatus()
        if status == .NotDetermined || status == .Denied || status == .AuthorizedWhenInUse {
            // present an alert indicating location authorization required
            // and offer to take the user to Settings for the app via
            // UIApplication -openUrl: and UIApplicationOpenSettingsURLString
            locationManager.requestAlwaysAuthorization()
            locationManager.requestWhenInUseAuthorization()
        }
        locationManager.startUpdatingLocation()
        locationManager.startUpdatingHeading()
        
        
        //mapview setup to show user location
        mapView.delegate = self
        mapView.showsUserLocation = true
        mapView.mapType = MKMapType(rawValue: 0)!
        mapView.userTrackingMode = MKUserTrackingMode(rawValue: 2)!
        
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        mapView.mapType = MKMapType(rawValue: 0)!
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        locationManager.startUpdatingHeading()
        locationManager.startUpdatingLocation()
    }
    
    override func viewWillDisappear(animated: Bool) {
        locationManager.stopUpdatingHeading()
        locationManager.stopUpdatingLocation()
    }
    
    // MARK :- CLLocationManager delegate
    func locationManager(manager: CLLocationManager!, didUpdateToLocation newLocation: CLLocation!, fromLocation oldLocation: CLLocation!) {
        
        //println("present location : \(newLocation.coordinate.latitude),\(newLocation.coordinate.longitude)")
        
        //drawing path or route covered
        if let oldLocationNew = oldLocation as CLLocation?{
            let oldCoordinates = oldLocationNew.coordinate
            let newCoordinates = newLocation.coordinate
            var area = [oldCoordinates, newCoordinates]
            let polyline = MKPolyline(coordinates: &area, count: area.count)
            drawRoute=0
            mapView.addOverlay(polyline)
        }
        
        
        //calculation for location selection for pointing annoation
        
    }
    
    // MARK :- MKMapView delegate
    func zoomToRegion() {
        var lat,lon:Double
        if selectedPath!.count==1{
            lat = stations[selectedPath![0].node]!.latitude
            lon = stations[selectedPath![0].node]!.longitude
        }
        else {
            var x = Int(ceil(Double(selectedPath!.count)/2.0))
            lat = stations[selectedPath![x].node]!.latitude
            lon = stations[selectedPath![x].node]!.longitude
        }
        
        
        let location = CLLocationCoordinate2D(latitude: lat, longitude: lon)
        let region = MKCoordinateRegionMakeWithDistance(location, 4000.0, 4000.0)
        mapView.setRegion(region, animated: true)
    }
    
    func mapView(mapView: MKMapView, rendererForOverlay overlay: MKOverlay) -> MKOverlayRenderer! {
        
        if (overlay is MKPolyline) {
            let pr = MKPolylineRenderer(overlay: overlay)
            pr.strokeColor = UIColor.redColor()
            pr.lineWidth = 5
            return pr
        }
        
        return nil
    }
    /*
    func routeDraw(){
        var routeStations : [Station2] = []
        
        //-----------------------------------------RouteDRAWNING-------------------------------------------------//
        
        for (var i = 0;i < selectedPath!.count;i++){
            var value = stations[selectedPath![i].node]
            let lat = value?.latitude
            let long = value?.longitude
            let annotation = Station2(latitude: lat!, longitude: long!)
            annotation.title = value!.title as? String
            routeStations.append(annotation)
        }
        
        
        var pointAnnotation:CustomPointAnnotation!
        
        var annotationView:MKPinAnnotationView!
        
        // Add mappoints to Map
        
        for j in routeStations{
            pointAnnotation = CustomPointAnnotation()
            pointAnnotation.pinCustomImageName = "tube"
            pointAnnotation.coordinate = CLLocationCoordinate2D(latitude: j.latitude, longitude: j.longitude)
            pointAnnotation.title = j.title! as String
            annotationView = MKPinAnnotationView(annotation: pointAnnotation, reuseIdentifier: "pin")
            mapView.addAnnotation(annotationView.annotation!)
            
        }
        
        //mapView.addAnnotations(ano)
        
        mapView.delegate = self
        
        // Connect all the mappoints using Poly line.
        
        
        
        var points: [CLLocationCoordinate2D] = [CLLocationCoordinate2D]()
        
        for station in routeStations {
            points.append(station.coordinate)
        }
        
        let polyline = MKPolyline(coordinates: &points, count: points.count)
        
        mapView.addOverlay(polyline)
    }
    
    func mapView(mapView: MKMapView,viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView?{
        
        let reuseIdentifier = "pin"
        
        var v = mapView.dequeueReusableAnnotationViewWithIdentifier(reuseIdentifier)
        if v == nil {
            v = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseIdentifier)
            v!.canShowCallout = true
        }
        else {
            v!.annotation = annotation
        }
        
        let customPointAnnotation = annotation as! CustomPointAnnotation
        v!.image = UIImage(named:customPointAnnotation.pinCustomImageName)
        
        return v
    }
    
    func mapView(mapView: MKMapView, rendererForOverlay overlay: MKOverlay) -> MKOverlayRenderer {
        
        let polylineRenderer = MKPolylineRenderer(overlay: overlay)
        
        if overlay is MKPolyline {
            
            if drawRoute==1{
            
            polylineRenderer.strokeColor = UIColor(hue: 0.5722, saturation: 0.92, brightness: 0.52, alpha: 1.0)
                polylineRenderer.lineWidth = 3}
            else{
                polylineRenderer.strokeColor = UIColor(hue: 0.9417, saturation: 0.84, brightness: 0.66, alpha: 1.0)
                polylineRenderer.lineWidth = 3
            }
            
        }
        
        
        return polylineRenderer
    }*/
    
    
}