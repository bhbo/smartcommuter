//
//  CustomPointAnnotation.swift
//  MapViewDemo
//
//  Created by Jirapa Lapanant on 2/17/2559 BE.
//  Copyright © 2559 Ravi Shankar. All rights reserved.
//

import Foundation
import UIKit
import MapKit

class CustomPointAnnotation: MKPointAnnotation {
    var pinCustomImageName:String!
}