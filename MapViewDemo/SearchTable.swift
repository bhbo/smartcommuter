//
//  SearchTable.swift
//  MapViewDemo
//
//  Created by Bo Watthanavarangkul on 3/14/2559 BE.
//  Copyright © 2559 Ravi Shankar. All rights reserved.
//

import MapKit
import UIKit
import Foundation

class SearchTable : UITableViewController {
    
    var filteredPlaces:[Place] = []
    
    var matchingPlaces:[Place] = []
    //var tableStationData : [String] = []
    var allTableData : [Place] = []
    //test
    //var filteredPlaces:[String] = []
    
    var handleMapSearchDelegate:HandleMapSearch? = nil
    
    var mapView: MKMapView? = nil
    
    var matchingItems:[MKMapItem] = []
    
    //var resultSearchController:UISearchController!
    
    //ku aim ka
    func parseAddress(selectedItem:MKPlacemark) -> String {
        
    // put a space between "4" and "Melrose Place"
        let firstSpace = (selectedItem.subThoroughfare != nil && selectedItem.thoroughfare != nil) ? " " : ""
        // put a comma between street and city/state
        let comma = (selectedItem.subThoroughfare != nil || selectedItem.thoroughfare != nil) && (selectedItem.subAdministrativeArea != nil || selectedItem.administrativeArea != nil) ? ", " : ""
        // put a space between "Washington" and "DC"
        let secondSpace = (selectedItem.subAdministrativeArea != nil && selectedItem.administrativeArea != nil) ? " " : ""
        let addressLine = String(
            format:"%@%@%@%@%@%@%@",
            // street number
            selectedItem.subThoroughfare ?? "",
            firstSpace,
            // street name
            selectedItem.thoroughfare ?? "",
            comma,
            // city
            selectedItem.locality ?? "",
            secondSpace,
            // state
            selectedItem.administrativeArea ?? ""
            
        )
        
        return addressLine
        
    }
    
    override func viewWillAppear(animated: Bool) {
        //clearsSelectionOnViewWillAppear = splitViewController!.collapsed
        super.viewWillAppear(animated)
        /*
        resultSearchController = UISearchController(searchResultsController: nil)
        // 2
        resultSearchController.searchResultsUpdater = self
        // 3
        resultSearchController.hidesNavigationBarDuringPresentation = false
        // 4
        resultSearchController.dimsBackgroundDuringPresentation = false
        // 5
        resultSearchController.searchBar.searchBarStyle = UISearchBarStyle.Prominent
        // 6
        resultSearchController.searchBar.sizeToFit()
        // 7
        self.tableView.tableHeaderView = resultSearchController.searchBar
 */
    }
    
}


// test 12334567890
// test 23320329-23-23-04

extension SearchTable : UISearchResultsUpdating {
    
    func updateSearchResultsForSearchController(searchController: UISearchController) {
      
        
        
        guard let mapView = mapView,
            let searchBarText = searchController.searchBar.text else { print("helllo");return }
        
        let request = MKLocalSearchRequest()
        request.naturalLanguageQuery = searchBarText
        request.region = mapView.region
        
        let search = MKLocalSearch(request: request)
        search.startWithCompletionHandler { response, error in
            guard let response = response else {
                print("There was an error searching for: \(request.naturalLanguageQuery) error: \(error)")
                return
            }
            self.matchingItems = response.mapItems
            
            var matchingPlaces:[Place] = []
            for item in response.mapItems {
                // Display the received items
                let title = item.placemark.name
                let lat = item.placemark.coordinate.latitude
                let lon = item.placemark.coordinate.longitude
                let place = Place(title: title!,latitude: lat,longitude: lon)
                
                self.matchingPlaces.append(place)
            }
            //self.matchingItems = response.mapItems
            
        }
        

        //get matching local search and append in Place Array
        //self.retriveApiPlaces(searchController.searchBar.text!)
       
        /*
        for(var i = 0; i<self.matchingItems.count;i++){
            print("hello")
            let title = self.matchingItems[i].placemark.title
            let lat = self.matchingItems[i].placemark.coordinate.latitude
            let lon = self.matchingItems[i].placemark.coordinate.longitude
            let place = Place(title: title!,latitude: lat,longitude: lon)
            
            self.allTableData.append(place)
            print(self.allTableData[i])
        }*/

        let searchBar = searchController.searchBar
        let scope = searchBar.scopeButtonTitles![searchBar.selectedScopeButtonIndex]
        self.filterContentForSearchText(searchController.searchBar.text!, scope: scope)
        
        //===================
        
        
        
    }
    
    
    
    func filterContentForSearchText(searchText: String, scope: String = "All") {
        
        var allData = allTableData+matchingPlaces
        filteredPlaces = allData.filter { candy in
            let categoryMatch = (scope == "All") || (candy.type == scope)
            return  categoryMatch && candy.title.lowercaseString.containsString(searchText.lowercaseString)
            
        }
         self.tableView.reloadData()
        
        
    }
    func retriveApiPlaces(searchText1: String){
        
        
    }
    
    
    
}



extension SearchTable {
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredPlaces.count
    }
    
    
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("cell")!
        let selectedItem = filteredPlaces[indexPath.row]
        //let selectedPlace = matchingItems[indexPath.row].placemark
        cell.textLabel?.text = selectedItem.title
        /*
        if (selectedItem.type=="Place"){
            cell.detailTextLabel?.text = parseAddress(selectedPlace)
        }
        else{
            cell.detailTextLabel?.text = selectedItem.type
        }*/
        cell.detailTextLabel?.text = selectedItem.type
        return cell
        
    }
    
}



extension SearchTable {
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        let selectedItem = filteredPlaces[indexPath.row]
        
        handleMapSearchDelegate?.dropPinZoomIn(selectedItem)
        
        dismissViewControllerAnimated(true, completion: nil)
        
        //searchController.searchResultsUpdater = self
        //searchController.searchBar.delegate = self
        definesPresentationContext = true
        //searchController.dimsBackgroundDuringPresentation = false
        
        // Setup the Scope Bar
        
    }
    
}





extension SearchTable: UISearchBarDelegate {
    
    func searchBar(searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        
        filterContentForSearchText(searchBar.text!, scope: searchBar.scopeButtonTitles![selectedScope])
        
    }
    
}