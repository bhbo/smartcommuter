//
//  Place.swift
//  MapViewDemo
//
//  Created by Bo Watthanavarangkul on 4/12/2559 BE.
//  Copyright © 2559 Ravi Shankar. All rights reserved.
//

import Foundation


import MapKit

class Place{
    var title: String
    var latitude: Double
    var longitude:Double
    var type : String = "Place"
    init(title: String,latitude: Double, longitude: Double) {
        
        self.latitude = latitude
        self.longitude = longitude
        self.title = title

    }
    
    var coordinate: CLLocationCoordinate2D {
        return CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
    }
    
    func description() ->String{
        let str = "\(title) at \(latitude),\(longitude)"
        return str
    }
}