//
//  CustomCell.swift
//  MapViewDemo
//
//  Created by Jirapa Lapanant on 4/3/2559 BE.
//  Copyright © 2559 Ravi Shankar. All rights reserved.
//

import Foundation
import UIKit

class CustomCell: UITableViewCell {
    
    
    @IBOutlet weak var lineLabel: UILabel!
    
    @IBOutlet weak var stationLabel: UILabel!
    
    @IBOutlet weak var photo: UIImageView!
    
    @IBOutlet weak var timeLabel: UILabel!
    
    //@IBOutlet weak var lineLabel: UILabel!
    //@IBOutlet weak var stationLabel: UILabel!
    //@IBOutlet weak var photo: UIImageView!
    //@IBOutlet weak var timeLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
