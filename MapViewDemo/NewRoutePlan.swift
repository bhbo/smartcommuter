//
//  NewRoutePlan.swift
//  MapViewDemo
//
//  Created by Jirapa Lapanant on 4/28/2559 BE.
//  Copyright © 2559 Ravi Shankar. All rights reserved.
//

import Foundation
import UIKit

class NewRoutePlan: UIViewController{
    
    var titleString: String!
    var previewPath : [(line:Int?,node:Int,time:Double)]!
    
    @IBOutlet weak var titleLabel: UILabel!
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.titleLabel.text = self.titleString
        
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */


}