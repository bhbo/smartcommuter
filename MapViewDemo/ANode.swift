//
//  ANode.swift
//  MapViewDemo
//
//  Created by Bo Watthanavarangkul on 3/6/2559 BE.
//  Copyright © 2559 Ravi Shankar. All rights reserved.
//

import Foundation

class ANode {
    
    var to_node_id :Int
    var line_id : Int?
    var parentNode: ANode?
    var hCost,time,gCost: Double
    
    init(line_id : Int?, to_node_id : Int, time:Double , parentNode : ANode?, gCost : Double, hCost : Double){
        self.line_id = line_id
        self.to_node_id = to_node_id
        self.gCost = gCost
        self.hCost = hCost
        self.time = time
        self.parentNode = parentNode
    }
    
    func getLine_id() -> Int?
    {
        return self.line_id
    }
    
    func getTo_node_id() ->Int{
        return self.to_node_id
    }
    
    func getParentNode()-> ANode{
        return self.parentNode!
    }
    
    func getGCost() -> Double{
        return self.gCost
    }
    
    func getHCost() ->Double {
        return self.hCost
    }
    
    
    func getEvalCost() -> Double{
        return self.gCost + self.hCost
    }
    
    func hasParentNode() -> Bool{
        
        return self.parentNode != nil
    }
    
}