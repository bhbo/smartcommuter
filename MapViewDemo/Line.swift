//
//  Line.swift
//  MapViewDemo
//
//  Created by Bo Watthanavarangkul on 3/6/2559 BE.
//  Copyright © 2559 Ravi Shankar. All rights reserved.
//

import Foundation

class Line{
    var name : String
    
    init (name:String){
        self.name=name
    }
}