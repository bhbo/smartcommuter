//
//  Protocol.swift
//  MapViewDemo
//
//  Created by Bo Watthanavarangkul on 5/16/2559 BE.
//  Copyright © 2559 Ravi Shankar. All rights reserved.
//

import Foundation
protocol HandleSelectedPath: class {
    func selectedPathDraw(selectedRoute:[(line:Int?,node:Int,time:Double)])
}