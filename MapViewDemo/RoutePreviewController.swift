//
//  RoutePreviewController.swift
//  MapViewDemo
//
//  Created by Jirapa Lapanant on 4/3/2559 BE.
//  Copyright © 2559 Ravi Shankar. All rights reserved.
//

import Foundation
import MapKit
import CoreData
import UIKit

class RoutePreviewController: UIViewController,UITableViewDataSource,UITableViewDelegate,MKMapViewDelegate{
    
    var previewPath : [(line:Int?,node:Int,time:Double)]!
    
    
    @IBAction func navigateButtonAction(sender: AnyObject) {
        let date = NSDate()
        let calendar = CalendarController()
        let cvDate = CVDate(date: date)
        
        
        var title = "You went to \(stations[previewPath![(previewPath!.count)-1].node]!.title)"
        let appDelegate =
            UIApplication.sharedApplication().delegate as! AppDelegate
        
        let managedContext = appDelegate.managedObjectContext!
        
        //2
        let entity =  NSEntityDescription.entityForName("Event",
                                                        inManagedObjectContext:
            managedContext)
        
        let event = NSManagedObject(entity: entity!,
                                    insertIntoManagedObjectContext:managedContext)
        
        
        
        //3
        event.setValue(title, forKey: "title")
        event.setValue(cvDate.day, forKey: "day")
        event.setValue(cvDate.month, forKey: "month")
        event.setValue(cvDate.year, forKey: "year")
        print("day:\(cvDate.day) month:\(cvDate.month) year:\(cvDate.year) week:\(cvDate.week)")
        //4
        var error: NSError?
        do {
            try managedContext.save()
        } catch let error1 as NSError {
            error = error1
            print("Could not save \(error), \(error?.userInfo)")
        }

        
        selectedPath = previewPath
        self.performSegueWithIdentifier("toTracking", sender: self)
        
    }
    
    
    @IBOutlet weak var mapView: MKMapView!
    /*
    @IBAction func BackToMap(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil);
    }
    */
    
    //weak var delegate: HandleSelectedPath?
    /*
    @IBAction func navigateButtonAction(sender: AnyObject) {
        /*
        delegate?.selectedPathDraw(previewPath)
        dismissViewControllerAnimated(true, completion: nil)
        definesPresentationContext = true
        */
        self.performSegueWithIdentifier("toTracking", sender: sender)
        
    }*/
    
    
    
    @IBOutlet weak var tableView: UITableView!
    
    
    //@IBOutlet weak var labelTest: UILabel!
    
    var stationArray:Array<String> = []
    var lineArray:Array<String> = []
    var imageIndex:Array<Int> = []
    //let start = stations[path![0].node]?.title
    //let goal = stations[path![(path?.count)!-1].node]?.title
    
    
    
    var linesImages = [1 : UIImage(named: "Bakerloo"),
                       2 : UIImage(named: "Central"),
                       3 : UIImage (named: "Circlenode"),
                       4 : UIImage(named: "District"),
                       6 : UIImage(named: "Jubilee"),
                       7 : UIImage(named: "Metropolitan"),
                       8 : UIImage(named: "Northern"),
                       9 : UIImage(named: "Piccadilly"),
                       10 : UIImage(named: "Victoria")]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        zoomToRegion()
        routeDraw()
      
        
        var totalTime: Double = 0;
        
        for (var i = 1;i < previewPath!.count;i++){
            stationArray.append((stations[previewPath![i].node]?.title)!)
            lineArray.append((lines[previewPath![i].line!]?.name)!)
            imageIndex.append(previewPath![i].line!)
            //totalTime+=path![i].time;
            if i>=1{
                
                print("Using \(lines[previewPath[i].line!]!.name) line to \(stations[previewPath[i].node]!.title)")}
    
        }
        
    
        
        
    }
    
    func zoomToRegion() {
        var lat,lon:Double
        if previewPath.count==1{
            lat = stations[previewPath[0].node]!.latitude
            lon = stations[previewPath[0].node]!.longitude
        }
        else {
            var x = Int(ceil(Double(previewPath.count)/2.0))
            lat = stations[previewPath[x].node]!.latitude
            lon = stations[previewPath[x].node]!.longitude
        }
        
                
        let location = CLLocationCoordinate2D(latitude: lat, longitude: lon)
        let region = MKCoordinateRegionMakeWithDistance(location, 4000.0, 4000.0)
        mapView.setRegion(region, animated: true)
    }
    
    func routeDraw(){
        var routeStations : [Station2] = []
        
        //-----------------------------------------RouteDRAWNING-------------------------------------------------//
        
         for (var i = 0;i < previewPath!.count;i++){
         var value = stations[previewPath![i].node]
         let lat = value?.latitude
         let long = value?.longitude
         let annotation = Station2(latitude: lat!, longitude: long!)
         annotation.title = value!.title as? String
         routeStations.append(annotation)
         }
 
        
        var pointAnnotation:CustomPointAnnotation!
        
        var annotationView:MKPinAnnotationView!
        
        // Add mappoints to Map
        
        for j in routeStations{
            pointAnnotation = CustomPointAnnotation()
            pointAnnotation.pinCustomImageName = "tube"
            pointAnnotation.coordinate = CLLocationCoordinate2D(latitude: j.latitude, longitude: j.longitude)
            pointAnnotation.title = j.title! as String
            annotationView = MKPinAnnotationView(annotation: pointAnnotation, reuseIdentifier: "pin")
            mapView.addAnnotation(annotationView.annotation!)
            
        }
        
        //mapView.addAnnotations(ano)
        
        mapView.delegate = self
        
        // Connect all the mappoints using Poly line.
        
        
        
        var points: [CLLocationCoordinate2D] = [CLLocationCoordinate2D]()
        
        for station in routeStations {
            points.append(station.coordinate)
        }
        
        let polyline = MKPolyline(coordinates: &points, count: points.count)
        
        mapView.addOverlay(polyline)
    }

    func mapView(mapView: MKMapView,viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView?{
        
        let reuseIdentifier = "pin"
        
        var v = mapView.dequeueReusableAnnotationViewWithIdentifier(reuseIdentifier)
        if v == nil {
            v = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseIdentifier)
            v!.canShowCallout = true
        }
        else {
            v!.annotation = annotation
        }
        
        let customPointAnnotation = annotation as! CustomPointAnnotation
        v!.image = UIImage(named:customPointAnnotation.pinCustomImageName)
        
        return v
    }
    
    func mapView(mapView: MKMapView, rendererForOverlay overlay: MKOverlay) -> MKOverlayRenderer {
        
        let polylineRenderer = MKPolylineRenderer(overlay: overlay)
        
        if overlay is MKPolyline {
            
            polylineRenderer.strokeColor = UIColor(hue: 0.5722, saturation: 0.92, brightness: 0.52, alpha: 1.0)
            polylineRenderer.lineWidth = 3
            
        }
        return polylineRenderer
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (previewPath?.count)!-1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) as! CustomCell
        cell.photo.image = linesImages[imageIndex[indexPath.row]]!
        cell.stationLabel.text = stationArray[indexPath.row]
        cell.lineLabel.text = lineArray[indexPath.row]
        
        return cell
        
        
    }
    
    override func viewWillAppear(animated: Bool) {
        //clearsSelectionOnViewWillAppear = splitViewController!.collapsed
        super.viewWillAppear(animated)
    }
}



