//
//  RoutePlanController.swift
//  MapViewDemo
//
//  Created by Jirapa Lapanant on 4/28/2559 BE.
//  Copyright © 2559 Ravi Shankar. All rights reserved.
//

import Foundation
import UIKit
import PhotosUI


class RoutePlanController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    
    

    @IBAction func backToMainPage(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil);

    }
    
    @IBOutlet weak var tableView: UITableView!
    var objects: NSMutableArray! = NSMutableArray()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
       
        
        //let imageName = "yourImage.png"
        
       
        
        var linesImages = [1 : UIImage(named: "Bakerloo"),
                           2 : UIImage(named: "Central"),
                           3 : UIImage (named: "Circlenode"),
                           4 : UIImage(named: "District"),
                           6 : UIImage(named: "Jubilee"),
                           7 : UIImage(named: "Metropolitan"),
                           8 : UIImage(named: "Northern"),
                           9 : UIImage(named: "Piccadilly"),
                           10 : UIImage(named: "Victoria")]
        var ySpace=0
        
        for var j in (0..<3){
            
            var xSpace = 0
            var totalTimeTravel = 0.0
            for (var i = 1;i < path![j].count;i += 1){
                if i>1 && path![j][i].line == path![j][i-1].line{
                     continue
                }
                var image = linesImages[path![j][i].line!]
                var imageView = UIImageView(image: image!)
                imageView.frame = CGRect(x: 20+xSpace, y: 105+ySpace, width: 15, height: 15)
                imageView.contentMode = .ScaleAspectFit
                xSpace+=25
                view.addSubview(imageView)
                totalTimeTravel+=path![j][i].time

            }
            self.objects.addObject("Plan \(j+1) : \(totalTimeTravel) mins")
            
            ySpace+=100
        }
        
        
        self.tableView.reloadData()
        
        
        
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table View Delegate
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.objects.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        
        let cell = self.tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) as! TableViewCell
        
        cell.tittleLabel.text = self.objects.objectAtIndex(indexPath.row) as? String
        
        cell.shareButton.tag = indexPath.row
        
        cell.shareButton.addTarget(self, action: "logAction:", forControlEvents: .TouchUpInside)
        
        return cell
        
    
        
        
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        self.performSegueWithIdentifier("showView", sender: self)
        
    }
    
    
    @IBAction func logAction(sender: UIButton) {
        
        let titleString = self.objects[sender.tag] as? String
        
        let firstActivityItem = "00\(titleString!)"
        
        let activityViewController : UIActivityViewController = UIActivityViewController(activityItems: [firstActivityItem], applicationActivities: nil)
        
        self.presentViewController(activityViewController, animated: true, completion: nil)
        
    }
    
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?)
    {
        if (segue.identifier == "showView")
        {
            let upcoming: RoutePreviewController = segue.destinationViewController as! RoutePreviewController
            
            let indexPath = self.tableView.indexPathForSelectedRow!
            
            //let titleString = self.objects.objectAtIndex(indexPath.row) as? String
            print(indexPath.row)
            
            //upcoming.titleString = titleString
            upcoming.previewPath = path![indexPath.row]
            
            self.tableView.deselectRowAtIndexPath(indexPath, animated: true)
        }
        
    }

    
    //commit manow
    
    
}